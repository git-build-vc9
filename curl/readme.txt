===============================================================================
    curl 7.19.0
===============================================================================

The is a modified curl to compile against the tinycrt. The following is the
description of the modified directory layout:

src\		the original source files necessary to build the library
inc\		the original public header files
readme.txt	this file
curl.vcproj	the visual studio 9 project file

All unecessary files were striped.

Every time the curl is updated against the official sources, the tree is left
exactly like the original, in the following commit we strip the tree to suit
the needs for the build and include the visual studio project file and this
readme and finally in the following commits we make necessary source code
modifications.

///////////////////////////////////////////////////////////////////////////////
