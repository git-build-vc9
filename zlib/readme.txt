===============================================================================
    zlib 1.2.3
===============================================================================

The is a modified zlib to compile against the tinycrt. The following is the
description of the modified directory layout:

src\		the original source files necessary to build the library
inc\		the original public header files
readme.txt	this file
zlib.vcproj	the visual studio 9 project file

Every time the zlib is updated against the official sources, the tree is left
exactly like the original, in the following commit we strip the tree to suit
the needs for the build and include the visual studio project file and this
readme and finally in the following commits we make necessary source code
modifications.

///////////////////////////////////////////////////////////////////////////////
