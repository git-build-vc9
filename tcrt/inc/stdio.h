/*=============================================================================
	stdio.h : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#ifndef TCRT_STDIO__H_
#define TCRT_STDIO__H_

///////////////////////////////////////////////////////////////////////////////
#include "tcrt.h"
#include <stdarg.h>

///////////////////////////////////////////////////////////////////////////////
TCRT_BEGIN_EXTERN_C

///////////////////////////////////////////////////////////////////////////////
#define	EOF		0x80
#define	FILENAME_MAX	260
#define	TMP_MAX		0x00FFFFFF

#define	_IOFBF		0x0001
#define	_IOLBF		0x0002
#define	_IONBF		0x0004

#define	BUFSIZ		4096
#define	FOPEN_MAX	0x3FFFFFFF
#define	L_tmpnam	(sizeof("/tmp/") + 20)

#define	SEEK_SET	0
#define	SEEK_CUR	1
#define	SEEK_END	2

///////////////////////////////////////////////////////////////////////////////
#ifdef __cplusplus
	struct FILE;
#else
	typedef struct _FILE FILE;
#endif

typedef long long fpos_t;

extern FILE* const stdin;
extern FILE* const stdout;
extern FILE* const stderr;

///////////////////////////////////////////////////////////////////////////////
int remove(const char* filename);
int rename(const char* oldname, const char* newname);
FILE* tmpfile(void);
char* tmpnam(char* str);

FILE* fopen(const char* filename, const char* mode);
FILE* freopen(const char* filename, const char* mode);
FILE* fdopen(int fildes, const char* mode);
int fileno(FILE* stream);
int fflush(FILE* stream);
int fclose(FILE* stream);
void setbuf(FILE* stream, char* buffer);
int setvbuf(FILE* stream, char* buffer, int mode, size_t size);

int sprintf(char* dst, const char* format, ...);
TCRT_IMPORT int vsprintf(char* dst, const char* format, va_list arg);
int snprintf(char* dst, size_t len, const char* format, ...);
int vsnprintf(char* dst, size_t len, const char* format, va_list arg);
TCRT_IMPORT int sscanf(const char* str, const char* format, ...);
int printf(const char* format, ...);
int vprintf(const char* format, va_list arg);
int scanf(const char* format, ...);
int fprintf(FILE* stream, const char* format, ...);
int vfprintf(FILE* stream, const char* format, va_list arg);
int fscanf(FILE* stream, const char* format, ...);

int fgetc(FILE* stream);
char* fgets(char* str, int num, FILE * stream);
int fputc(int character, FILE * stream);
int fputs(const char* str, FILE * stream);
int getc(FILE* stream);
int getchar(void);
char* gets(char* str);
int putc(int character, FILE* stream);
int putchar(int character);
int puts(const char* str);
int ungetc(int character, FILE* stream);

size_t fread(void * ptr, size_t size, size_t count, FILE* stream);
size_t fwrite(const void* ptr, size_t size, size_t count, FILE* stream);

int fgetpos(FILE* stream, fpos_t* position);
int fseek(FILE* stream, long long offset, int origin);
int fsetpos(FILE* stream, const fpos_t* pos);
long long ftell(FILE* stream);
void rewind(FILE* rewind);

void clearerr(FILE* stream);
int feof(FILE* stream);
int ferror(FILE* stream);
void perror(const char* str);

///////////////////////////////////////////////////////////////////////////////
TCRT_END_EXTERN_C

// EOF ////////////////////////////////////////////////////////////////////////
#endif /* TCRT_STDIO_H */
