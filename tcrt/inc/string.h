/*=============================================================================
	tcrt.h : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#ifndef TCRT_STRING__H_
#define TCRT_STRING__H_

///////////////////////////////////////////////////////////////////////////////
#include "tcrt.h"

///////////////////////////////////////////////////////////////////////////////
TCRT_BEGIN_EXTERN_C

///////////////////////////////////////////////////////////////////////////////
TCRT_IMPORT void* memcpy(void* dst, const void* src, size_t size);
TCRT_IMPORT void* memmove(void* dst, const void* src, size_t size);
TCRT_IMPORT void* memset(void* mem, int value, size_t size);
TCRT_IMPORT void* memchr(const void* mem, int value, size_t size);
TCRT_IMPORT int memcmp(const void* mem1, const void* mem2, size_t size);

TCRT_IMPORT size_t strlen(const char* str);
TCRT_IMPORT size_t strnlen(const char* str, size_t len);

TCRT_IMPORT char* strcpy(char* dst, const char* src);
TCRT_IMPORT char* strncpy(char* dst, const char* src, size_t len);
TCRT_IMPORT char* strcat(char* dst, const char* src);
TCRT_IMPORT char* strncat(char* dst, const char* src, size_t len);
TCRT_IMPORT int strcmp(const char* str1, const char* str2);
TCRT_IMPORT int strncmp(const char* str1, const char* str2, size_t len);

TCRT_IMPORT char* strcoll(const char* str1, const char* str2);
TCRT_IMPORT size_t strxfrm(char * destination, const char * source, size_t num);

TCRT_IMPORT char* strchr(const char* str, int value);
TCRT_IMPORT size_t strcspn(const char* str, const char* cmp);
TCRT_IMPORT char* strpbrk(const char* str1, const char* str2);
TCRT_IMPORT char* strrchr(const char* str, int value);
TCRT_IMPORT size_t strspn(const char* str, const char* cmp);
TCRT_IMPORT char* strstr(const char* str1, const char* str2);
TCRT_IMPORT char* strtok(char* str, const char* delimiters);

char* strerror(int errnum);

TCRT_IMPORT size_t wcslen(wchar_t* str);
TCRT_IMPORT wchar_t* wcscpy(wchar_t* dst, const wchar_t* src);

///////////////////////////////////////////////////////////////////////////////
char* strdup(const char* str);
int stricmp(const char* str1, const char* str2);
int strnicmp(const char* str1, const char* str2, size_t len);
int strcasecmp(const char* str1, const char* str2);
int strncasecmp(const char* str1, const char* str2, size_t len);

///////////////////////////////////////////////////////////////////////////////
TCRT_END_EXTERN_C

// EOF ////////////////////////////////////////////////////////////////////////
#endif /* TCRT_STRING__H_ */
