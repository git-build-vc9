/*=============================================================================
	stdlib.h : 

	Copyright � 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#ifndef TCRT_STDLIB__H_
#define TCRT_STDLIB__H_

///////////////////////////////////////////////////////////////////////////////
#include "tcrt.h"

///////////////////////////////////////////////////////////////////////////////
TCRT_BEGIN_EXTERN_C

///////////////////////////////////////////////////////////////////////////////
#define EXIT_SUCCESS	0
#define EXIT_FAILURE	1

#define MB_CUR_MAX	4

#define RAND_MAX	0x7FFF

///////////////////////////////////////////////////////////////////////////////
typedef struct div_t {
	int quot;
	int rem;
} div_t;

///////////////////////////////////////////////////////////////////////////////
char* mktemp(char* templat);
int mkstemp(char* templat);
int setenv(const char* envname, const char* envval, int overwrite);
int unsetenv(const char* name);
int putenv(char* string);

///////////////////////////////////////////////////////////////////////////////
TCRT_IMPORT double atof(const char* str);
TCRT_IMPORT int atoi(const char* str);
TCRT_IMPORT long atol(const char* str);
TCRT_IMPORT long strtod(const char* str, char** endptr);
TCRT_IMPORT long strtol(const char* str, char** endptr, int base);
TCRT_IMPORT unsigned long strtoul(const char* str, char** endptr, int base);
long long strtoll(const char* str, char** endptr, int base);
unsigned long long strtoull(const char* str, char** endptr, int base);

TCRT_IMPORT int rand(void);
TCRT_IMPORT void srand(unsigned int seed);

void* malloc(size_t size);
void* calloc(size_t num, size_t size);
void* realloc(void* ptr, size_t size);
void free(void* ptr);

TCRT_NORETURN void abort(void);
int atexit(void (*function)(void));
TCRT_NORETURN void exit(int status);
char* getenv(const char* name);
int system(const char* cmd);

TCRT_IMPORT void* bsearch(const void* key, const void* base, size_t num, size_t size, int (*comparator)(const void*, const void*));
TCRT_IMPORT void qsort(void* base, size_t num, size_t size, int (*comparator)(const void*, const void*));

int abs(int value);
long labs(long value);
div_t div(int numerator, int denominator);

int mblen(const char* pmb, size_t max);
int mbtowc(wchar_t* pwc, const char* pmb, size_t max);
int wctomb(char* pmb, wchar_t character);

size_t mbstowcs(wchar_t* wcstr, const char* mbstr, size_t max);
size_t wcstombs(char* mbstr, const wchar_t* wcstr, size_t max);

///////////////////////////////////////////////////////////////////////////////
TCRT_END_EXTERN_C

// EOF ////////////////////////////////////////////////////////////////////////
#endif /* TCRT_STDLIB__H_ */
