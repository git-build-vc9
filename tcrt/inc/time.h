/*=============================================================================
	time.h : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#ifndef TCRT_TIME__H_
#define TCRT_TIME__H_

///////////////////////////////////////////////////////////////////////////////
#include "tcrt.h"

///////////////////////////////////////////////////////////////////////////////
TCRT_BEGIN_EXTERN_C

///////////////////////////////////////////////////////////////////////////////
#ifndef _TIME_T_DEFINED
typedef long long time_t;
#define _TIME_T_DEFINED
#endif

#ifndef _CLOCK_T_DEFINED
typedef long long clock_t;
#define _CLOCK_T_DEFINED
#endif

#ifndef _TM_DEFINED
struct tm {
	int tm_sec;
	int tm_min;
	int tm_hour;
	int tm_mday;
	int tm_mon;
	int tm_year;
	int tm_wday;
	int tm_yday;
	int tm_isdst;
};
#define _TM_DEFINED
#endif

extern clock_t CLOCKS_PER_SEC;

///////////////////////////////////////////////////////////////////////////////
clock_t clock(void);
double difftime(time_t time2, time_t time1);
time_t mktime(struct tm* timeptr);
time_t time(time_t* timer);

char* asctime(const struct tm* timeptr);
char* ctime(const time_t* timer);
struct tm* gmtime(const time_t* timer);
struct tm* localtime(const time_t* timer);
size_t strftime(char* ptr, size_t maxsize, const char* format, const struct tm* timeptr);

///////////////////////////////////////////////////////////////////////////////
struct tm* localtime_r(const time_t* timer, struct tm* result);
struct tm* gmtime_r(const time_t* timer, struct tm* result);

///////////////////////////////////////////////////////////////////////////////
TCRT_END_EXTERN_C

// EOF ////////////////////////////////////////////////////////////////////////
#endif /* TCRT_TIME__H_ */
