/*=============================================================================
	signal.h : 

	Copyright � 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#ifndef TCRT_SIGNAL__H_
#define TCRT_SIGNAL__H_

///////////////////////////////////////////////////////////////////////////////
#include "tcrt.h"

///////////////////////////////////////////////////////////////////////////////
TCRT_BEGIN_EXTERN_C

///////////////////////////////////////////////////////////////////////////////
#define SIGABRT		0
#define SIGILL		1
#define SIGSEGV		2
#define SIGFPE		3
#define SIGINT		4
#define SIGTERM		5
#define SIGALRM		6
#define SIGMAX		7
#define SIGQUIT		SIGMAX
#define SIGHUP		SIGMAX
#define SIGPIPE		SIGMAX
#define SIGCHLD		SIGMAX

#define SIG_DFL		((void (*)(int)) 0)
#define SIG_IGN		((void (*)(int)) 1)
#define SIG_GET		((void (*)(int)) 2)
#define SIG_SGE		((void (*)(int)) 3)
#define SIG_ACK		((void (*)(int)) 4)
#define SIG_ERR		((void (*)(int)) -1)

#define SA_RESTART	0x0001

///////////////////////////////////////////////////////////////////////////////
typedef int		sig_atomic_t;
typedef unsigned int	sigset_t;

typedef struct sigaction {
	void		(*sa_handler)(int);
	sigset_t	sa_mask;
	int		sa_flags;
};

extern struct sigaction g_tcrt_sigaction_slot[SIGMAX];

///////////////////////////////////////////////////////////////////////////////
int sigaction(int sig, const struct sigaction* act, struct sigaction* oact);
int sigemptyset(sigset_t* set);
void (*signal(int sig, void (*func)(int)))(int);
int raise(int sig);

///////////////////////////////////////////////////////////////////////////////
TCRT_END_EXTERN_C

// EOF ////////////////////////////////////////////////////////////////////////
#endif /* TCRT_SIGNAL__H_ */
