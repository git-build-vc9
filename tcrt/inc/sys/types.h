/*=============================================================================
	types.h : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#ifndef TCRT_SYS_TYPES__H_
#define TCRT_SYS_TYPES__H_

///////////////////////////////////////////////////////////////////////////////
#include "../tcrt.h"

///////////////////////////////////////////////////////////////////////////////
TCRT_BEGIN_EXTERN_C

///////////////////////////////////////////////////////////////////////////////
typedef unsigned long	blkcnt_t;
typedef unsigned long	blksize_t;
typedef unsigned long	dev_t;
typedef unsigned long	gid_t;
typedef size_t		ino_t;
typedef unsigned long	mode_t;
typedef unsigned long	nlink_t;
typedef long long	off_t;
typedef int		pid_t;
typedef ptrdiff_t	ssize_t;
typedef unsigned long	uid_t;

///////////////////////////////////////////////////////////////////////////////
TCRT_END_EXTERN_C

// EOF ////////////////////////////////////////////////////////////////////////
#endif /* TCRT_SYS_TYPES__H_ */
