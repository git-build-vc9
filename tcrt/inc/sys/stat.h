/*=============================================================================
	stat.h : 

	Copyright � 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#ifndef TCRT_SYS_STAT__H_
#define TCRT_SYS_STAT__H_

///////////////////////////////////////////////////////////////////////////////
#include "../time.h"
#include "types.h"

///////////////////////////////////////////////////////////////////////////////
TCRT_BEGIN_EXTERN_C

///////////////////////////////////////////////////////////////////////////////
#define S_IFMT		00170000
#define S_IFSOCK	0140000
#define S_IFLNK		0120000
#define S_IFREG		0100000
#define S_IFBLK		0060000
#define S_IFDIR		0040000
#define S_IFCHR		0020000
#define S_IFIFO		0010000
#define S_ISUID		0004000
#define S_ISGID		0002000
#define S_ISVTX		0001000

#define S_ISLNK(m)	(((m) & S_IFMT) == S_IFLNK)
#define S_ISREG(m)	(((m) & S_IFMT) == S_IFREG)
#define S_ISDIR(m)	(((m) & S_IFMT) == S_IFDIR)
#define S_ISCHR(m)	(((m) & S_IFMT) == S_IFCHR)
#define S_ISBLK(m)	(((m) & S_IFMT) == S_IFBLK)
#define S_ISFIFO(m)	(((m) & S_IFMT) == S_IFIFO)
#define S_ISSOCK(m)	(((m) & S_IFMT) == S_IFSOCK)

#define S_IRWXU		00700
#define S_IRUSR		00400
#define S_IWUSR		00200
#define S_IXUSR		00100

#define S_IRWXG		00070
#define S_IRGRP		00040
#define S_IWGRP		00020
#define S_IXGRP		00010

#define S_IRWXO		00007
#define S_IROTH		00004
#define S_IWOTH		00002
#define S_IXOTH		00001

///////////////////////////////////////////////////////////////////////////////
typedef struct stat {
	dev_t		st_dev;
	ino_t		st_ino;
	mode_t		st_mode;
	uid_t		st_uid;
	gid_t		st_gid;
	off_t		st_size;
	time_t		st_atime;
	time_t		st_mtime;
	time_t		st_ctime;
	blksize_t	st_blksize;
	blkcnt_t	st_blocks;
};

///////////////////////////////////////////////////////////////////////////////
int chmod(const char* path, mode_t mode);
int fchmod(int fildes, mode_t mode);
int fstat(int fildes, struct stat* buf);
int mkdir(const char* path, mode_t mode);
int lstat(const char* path, struct stat* buf);
int stat(const char* path, struct stat* buf);
mode_t umask(mode_t mode);

///////////////////////////////////////////////////////////////////////////////
TCRT_END_EXTERN_C

// EOF ////////////////////////////////////////////////////////////////////////
#endif /* TCRT_SYS_STAT__H_ */
