/*=============================================================================
	mman.h : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#ifndef TCRT_SYS_MMAN__H_
#define TCRT_SYS_MMAN__H_

///////////////////////////////////////////////////////////////////////////////
#include "types.h"

///////////////////////////////////////////////////////////////////////////////
TCRT_BEGIN_EXTERN_C

///////////////////////////////////////////////////////////////////////////////
#define PROT_WRITE	0x0002
#define PROT_READ	0x0004
#define MAP_PRIVATE	1
#define MAP_FAILED	((void*)-1)

void* mmap(void* start, size_t length, int prot, int flags, int fd, off_t offset);
int munmap(void* start, size_t length);

///////////////////////////////////////////////////////////////////////////////
TCRT_END_EXTERN_C

// EOF ////////////////////////////////////////////////////////////////////////
#endif /* TCRT_SYS_MMAN__H_ */

