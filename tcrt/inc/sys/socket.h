/*=============================================================================
	socket.h : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#ifndef TCRT_SYS_SOCKET__H_
#define TCRT_SYS_SOCKET__H_

///////////////////////////////////////////////////////////////////////////////
#include "types.h"

///////////////////////////////////////////////////////////////////////////////
TCRT_BEGIN_EXTERN_C

///////////////////////////////////////////////////////////////////////////////
#define SOCKET_FILDES		0x40000000

#define INET_ADDRSTRLEN		22
#define INET6_ADDRSTRLEN	65

#define INADDR_ANY              (unsigned long)	0x00000000
#define INADDR_NONE             (unsigned long)	0xFFFFFFFF

#define AF_UNSPEC	0
#define AF_UNIX		1
#define AF_INET		2
#define AF_INET6	23

#define PF_UNSPEC	AF_UNSPEC
#define PF_UNIX		AF_UNIX
#define PF_INET		AF_INET
#define PF_INET6	AF_INET6

#define SOCK_STREAM	1
#define SOCK_DGRAM	2
#define SOCK_RAW	3

#define	IPPROTO_ICMP	1
#define	IPPROTO_IPV4	4
#define	IPPROTO_TCP	6
#define	IPPROTO_UDP	17
#define	IPPROTO_IPV6	41
#define	IPPROTO_ICMPV6	58
#define	IPPROTO_RAW	255


#define SOL_SOCKET		1

#define SO_BROADCAST		1
#define SO_CONDITIONAL_ACCEPT	2
#define SO_DEBUG		3
#define SO_DONTLINGER		4
#define SO_DONTROUTE		5
#define SO_GROUP_PRIORITY	6
#define SO_KEEPALIVE		7
#define SO_LINGER		8
#define SO_OOBINLINE		9
#define SO_RCVBUF		10
#define SO_REUSEADDR		11
#define SO_EXCLUSIVEADDRUSE	12
#define SO_SNDBUF		13
#define SO_NONBLOCK		14


#define HOST_NOT_FOUND          11001 //WSAHOST_NOT_FOUND
#define TRY_AGAIN               11002 //WSATRY_AGAIN
#define NO_RECOVERY             11003 //WSANO_RECOVERY
#define NO_DATA                 11004 //WSANO_DATA
#define NO_ADDRESS              NO_DATA

#define EWOULDBLOCK		10035 //WSAEWOULDBLOCK
#define EINPROGRESS		10036 //WSAEINPROGRESS
/*
#define EALREADY		WSAEALREADY
#define ENOTSOCK		WSAENOTSOCK
#define EDESTADDRREQ		WSAEDESTADDRREQ
#define EMSGSIZE		WSAEMSGSIZE
#define EPROTOTYPE		WSAEPROTOTYPE
#define ENOPROTOOPT		WSAENOPROTOOPT
#define EPROTONOSUPPORT		WSAEPROTONOSUPPORT
#define ESOCKTNOSUPPORT		WSAESOCKTNOSUPPORT
#define EOPNOTSUPP		WSAEOPNOTSUPP
#define EPFNOSUPPORT		WSAEPFNOSUPPORT
*/
#define EAFNOSUPPORT		10046 //WSAEAFNOSUPPORT
/*
#define EADDRINUSE		WSAEADDRINUSE
#define EADDRNOTAVAIL		WSAEADDRNOTAVAIL
#define ENETDOWN		WSAENETDOWN
#define ENETUNREACH		WSAENETUNREACH
#define ENETRESET		WSAENETRESET
#define ECONNABORTED		WSAECONNABORTED
#define ECONNRESET		WSAECONNRESET
#define ENOBUFS			WSAENOBUFS
#define EISCONN			WSAEISCONN
#define ENOTCONN		WSAENOTCONN
#define ESHUTDOWN		WSAESHUTDOWN
#define ETOOMANYREFS		WSAETOOMANYREFS
#define ETIMEDOUT		WSAETIMEDOUT
#define ECONNREFUSED		WSAECONNREFUSED
#define ELOOP			WSAELOOP
#define ENAMETOOLONG		WSAENAMETOOLONG
#define EHOSTDOWN		WSAEHOSTDOWN
#define EHOSTUNREACH		WSAEHOSTUNREACH
#define ENOTEMPTY		WSAENOTEMPTY
#define EPROCLIM		WSAEPROCLIM
#define EUSERS			WSAEUSERS
#define EDQUOT			WSAEDQUOT
#define ESTALE			WSAESTALE
#define EREMOTE			WSAEREMOTE
*/

#define EAI_MEMORY		8 //WSA_NOT_ENOUGH_MEMORY


///////////////////////////////////////////////////////////////////////////////
typedef struct sockaddr {
	unsigned short	sa_family;
	char		sa_data[14];
};

typedef struct in_addr {
	union {
		struct {
			unsigned char s_net;
			unsigned char s_host;
			unsigned char s_lh;
			unsigned char s_impno;
		};
		struct {
			unsigned short pad;
			unsigned short s_imp;
		};
		unsigned long s_addr;
	};
};

typedef struct in_addr6 {
	unsigned char	s6_addr[16];
};

typedef struct sockaddr_in {
	unsigned short	sin_family;
	unsigned short	sin_port;
	struct in_addr	sin_addr;
	char		sin_zero[8];
};

typedef struct sockaddr_in6 {
	unsigned short	sin6_family;
	unsigned short	sin6_port;
	unsigned long	sin6_flowinfo;
	struct in_addr6	sin6_addr;
	unsigned long	sin6_scope_id;
};

struct  hostent {
	char*		h_name;
	char**		h_aliases;
	short		h_addrtype;
	short		h_length;
	char**		h_addr_list;
#define h_addr		h_addr_list[0]
};

typedef struct addrinfo {
	int			ai_flags;
	int			ai_family;
	int			ai_socktype;
	int			ai_protocol;
	size_t			ai_addrlen;
	char*			ai_canonname;
	struct sockaddr*	ai_addr;
	struct addrinfo*	ai_next;
};

///////////////////////////////////////////////////////////////////////////////
unsigned long ntohl(unsigned long netlong);
unsigned long htonl(unsigned long hostlong);
unsigned short ntohs(unsigned short netshort);
unsigned short htons(unsigned short hostshort);

int socket(int af, int type, int protocol);
int accept(int sock, struct sockaddr* addr, size_t* addr_len);
int bind(int sock, const struct sockaddr* addr, size_t addr_len);
int connect(int sock, const struct sockaddr* addr, size_t addr_len);
int listen(int sock, int backlog);
ssize_t recv(int socket, void* buf, size_t len, int flags);
ssize_t recvfrom(int sock, void* buf, size_t len, int flags, struct sockaddr* addr, size_t* addr_len);
ssize_t send(int sock, const void *buf, size_t len, int flags);
ssize_t sendto(int sock, const void* buf, size_t len, int flags, const struct sockaddr* addr, size_t addr_len);
int setsockopt(int sock, int level, int option, const void* buf, size_t len);
int shutdown(int sock, int how);

int getsockname(int sock, struct sockaddr* name, size_t* name_len);
int gethostname(char* name, size_t len);
struct hostent* gethostbyname(const char* name);

int getaddrinfo(const char* node, const char* service, const struct addrinfo* hints, struct addrinfo** res);
void freeaddrinfo(struct addrinfo* res);
const char* gai_strerror(int errcode);

const char* inet_ntop(int af, const void* src, char* dst, size_t size);
int inet_pton(int af, const char* src, void* dst);
unsigned long inet_addr(const char* cp);
char* inet_ntoa(struct in_addr in);

///////////////////////////////////////////////////////////////////////////////
TCRT_END_EXTERN_C

// EOF ////////////////////////////////////////////////////////////////////////
#endif /* TCRT_SYS_SOCKET__H_ */
