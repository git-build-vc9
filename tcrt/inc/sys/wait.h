/*=============================================================================
	wait.h : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#ifndef TCRT_SYS_WAIT__H_
#define TCRT_SYS_WAIT__H_

///////////////////////////////////////////////////////////////////////////////
#include "types.h"

///////////////////////////////////////////////////////////////////////////////
TCRT_BEGIN_EXTERN_C

///////////////////////////////////////////////////////////////////////////////
#define WUNTRACED	0x0001
#define WEXITED		0x0002
#define WSTOPPED	0x0004
#define WCONTINUED	0x0008
#define WNOHANG		0x0010
#define WNOWAIT		0x0020

#define WEXITSTATUS(x)	((x) & 0xFF)
#define WIFEXITED(x)	((unsigned int)(x) < 259)
#define WIFSIGNALED(x)	((unsigned int)(x) > 259)

///////////////////////////////////////////////////////////////////////////////
pid_t waitpid(pid_t pid, int* stat_loc, int options);

///////////////////////////////////////////////////////////////////////////////
TCRT_END_EXTERN_C

// EOF ////////////////////////////////////////////////////////////////////////
#endif /* TCRT_SYS_WAIT__H_ */
