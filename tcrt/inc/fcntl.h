/*=============================================================================
	fcntl.h : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#ifndef TCRT_FCNTL__H_
#define TCRT_FCNTL__H_

///////////////////////////////////////////////////////////////////////////////
#include "sys/types.h"

///////////////////////////////////////////////////////////////////////////////
TCRT_BEGIN_EXTERN_C

///////////////////////////////////////////////////////////////////////////////
#define F_GETFD		1
#define F_SETFD		2
#define FD_CLOEXEC	0x0001

#define O_CREAT		0x0001
#define O_EXCL		0x0002
#define O_TRUNC		0x0004
#define O_APPEND	0x0008
#define O_BINARY	0x1000
#define O_SYMLINK	0x2000
#define O_DELETE	0x4000

#define O_RDONLY	0x0010
#define O_WRONLY	0x0020
#define O_RDWR		(O_RDONLY | O_WRONLY)

///////////////////////////////////////////////////////////////////////////////
int open(const char* path, int oflag, ...);
int fcntl(int fildes, int cmd, ...);

///////////////////////////////////////////////////////////////////////////////
TCRT_END_EXTERN_C

// EOF ////////////////////////////////////////////////////////////////////////
#endif /* TCRT_FCNTL__H_ */
