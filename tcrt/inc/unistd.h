/*=============================================================================
	unistd.h : 

	Copyright � 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#ifndef TCRT_UNISTD__H_
#define TCRT_UNISTD__H_

///////////////////////////////////////////////////////////////////////////////
#include "sys/types.h"

///////////////////////////////////////////////////////////////////////////////
TCRT_BEGIN_EXTERN_C

///////////////////////////////////////////////////////////////////////////////
#define STDIN_FILENO	0
#define STDOUT_FILENO	1
#define STDERR_FILENO	2

#define R_OK		0x0001
#define W_OK		0x0002
#define X_OK		0x0004
#define F_OK		0x0008

///////////////////////////////////////////////////////////////////////////////
int access(const char* path, int amode);
int chdir(const char* path);
int close(int fildes);
int dup(int fildes);
int dup2(int oldfd, int newfd);
int execl(const char* path, const char* arg0, ... /*, (char* )0 */);
int execlp(const char* file, const char* arg0, ... /*, (char* )0 */);
int execvp(const char* file, char* const argv[]);
pid_t fork(void);
int fsync(int fildes);
int ftruncate(int fildes, off_t length);
char* getcwd(char* buf, size_t size);
int getpagesize(void);
pid_t getpid(void);
uid_t getuid(void);
int isatty(int fildes);
int link(const char* path1, const char* path2);
off_t lseek(int fildes, off_t offset, int whence);
int pipe(int fildes[2]);
int readlink(const char* path, char* buf, size_t bufsize);
int rmdir(const char* path);
unsigned sleep(unsigned seconds);
int symlink(const char* path1, const char* path2);
int unlink(const char* path);

ssize_t read(int fildes, void* buf, size_t bufsize);
ssize_t pread(int fildes, void* buf, size_t bufsize, off_t offset);
ssize_t write(int fildes, const void* buf, size_t bufsize);
ssize_t pwrite(int fildes, const void *buf, size_t bufsize, off_t offset);

///////////////////////////////////////////////////////////////////////////////
TCRT_END_EXTERN_C

// EOF ////////////////////////////////////////////////////////////////////////
#endif /* TCRT_UNISTD__H_ */
