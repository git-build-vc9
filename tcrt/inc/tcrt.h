/*=============================================================================
	tcrt.h : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#ifndef TCRT__H_
#define TCRT__H_

///////////////////////////////////////////////////////////////////////////////
#define __STDC__			1
#define __STDC_VERSION__		199409
#define __STDC_HOSTED__			1
#define __TINYCRT__			1
#define PATH_MAX			260

#define TCRT_IMPORT			__declspec(dllimport)
#define TCRT_THREAD_LOCAL		__declspec(thread)
#define TCRT_NORETURN			__declspec(noreturn)
#define TCRT_STATIC_ASSERT(x)		typedef char __static_assert__[(x)]
#define TCRT_CONCAT_(a, b)		a ## b
#define TCRT_STRINGIFY_(x)		#x
#define TCRT_CONCAT(a, b)		TCRT_CONCAT_(a, b)
#define TCRT_STRINGIFY(x)		TCRT_STRINGIFY_(x)

#ifdef	__cplusplus
#	define TCRT_BEGIN_EXTERN_C	extern "C" {
#	define TCRT_END_EXTERN_C	}
#	ifndef	NULL
#		define NULL		0
#	endif
#else
#	define TCRT_BEGIN_EXTERN_C
#	define TCRT_END_EXTERN_C
#	ifndef	NULL
#		define NULL		((void*) 0)
#	endif
#endif

#ifndef _SIZE_T_DEFINED
#	ifdef	_M_IX86
	typedef unsigned int		size_t;
#	elif	_M_X64
	typedef unsigned __int64	size_t;
#	else
#		error unknown machine
#	endif
#	define	_SIZE_T_DEFINED
#endif

#ifndef	_PTRDIFF_T_DEFINED
#	ifdef	_M_IX86
	typedef signed int		ptrdiff_t;
#	elif	_M_X64
	typedef signed __int64		ptrdiff_t;
#	else
#		error unknown machine
#	endif
#	define	_PTRDIFF_T_DEFINED
#endif

#ifndef _WCHAR_T_DEFINED
	typedef unsigned short		wchar_t;
#define _WCHAR_T_DEFINED
#endif

#ifndef	_CRT_ERRNO_DEFINED
#define	_CRT_ERRNO_DEFINED
#else
#undef errno
#endif

#ifndef _ERRCODE_DEFINED
#define _ERRCODE_DEFINED
#endif

// EOF ////////////////////////////////////////////////////////////////////////
#endif /* TCRT__H_ */
