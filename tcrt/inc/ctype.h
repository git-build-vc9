/*=============================================================================
	stdlib.h : 

	Copyright � 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#ifndef TCRT_CTYPE__H_
#define TCRT_CTYPE__H_

///////////////////////////////////////////////////////////////////////////////
#include "tcrt.h"

///////////////////////////////////////////////////////////////////////////////
TCRT_BEGIN_EXTERN_C

///////////////////////////////////////////////////////////////////////////////
enum tcrt_ctype_flags {
	tcrt_ctype_flag_upper	= 0x01,
	tcrt_ctype_flag_lower	= 0x02,
	tcrt_ctype_flag_cntrl	= 0x04,
	tcrt_ctype_flag_digit	= 0x08,
	tcrt_ctype_flag_punct	= 0x10,
	tcrt_ctype_flag_xdigit	= 0x20,
	tcrt_ctype_flag_space	= 0x40,
	tcrt_ctype_flag_alpha	= (tcrt_ctype_flag_upper | tcrt_ctype_flag_lower),
	tcrt_ctype_flag_alnum	= (tcrt_ctype_flag_alpha | tcrt_ctype_flag_digit),
	tcrt_ctype_flag_graph	= (tcrt_ctype_flag_alnum | tcrt_ctype_flag_punct),
	tcrt_ctype_flag_print	= (tcrt_ctype_flag_space | tcrt_ctype_flag_graph),
};

///////////////////////////////////////////////////////////////////////////////
#undef tolower
#undef toupper
#undef isalpha
#undef isdigit
#undef isalnum
#undef isxdigit
#undef iscntrl
#undef isgraph
#undef isprint
#undef ispunct
#undef isspace
#undef islower
#undef isupper

///////////////////////////////////////////////////////////////////////////////
int tolower(int c);
int toupper(int c);
int tcrt_ctype(int c);

///////////////////////////////////////////////////////////////////////////////
static __inline int isalpha(int c)
{
	return (tcrt_ctype(c) & tcrt_ctype_flag_alpha);
}

static __inline int isdigit(int c)
{
	return (tcrt_ctype(c) & tcrt_ctype_flag_digit);
}

static __inline int isalnum(int c)
{
	return (tcrt_ctype(c) & tcrt_ctype_flag_alnum);
}

static __inline int isxdigit(int c)
{
	return (tcrt_ctype(c) & tcrt_ctype_flag_xdigit);
}

static __inline int iscntrl(int c)
{
	return (tcrt_ctype(c) & tcrt_ctype_flag_cntrl);
}

static __inline int isgraph(int c)
{
	return (tcrt_ctype(c) & tcrt_ctype_flag_graph);
}

static __inline int isprint(int c)
{
	return (tcrt_ctype(c) & tcrt_ctype_flag_print);
}

static __inline int ispunct(int c)
{
	return (tcrt_ctype(c) & tcrt_ctype_flag_punct);
}

static __inline int isspace(int c)
{
	return (tcrt_ctype(c) & tcrt_ctype_flag_space);
}

static __inline int islower(int c)
{
	return (tcrt_ctype(c) & tcrt_ctype_flag_lower);
}

static __inline int isupper(int c)
{
	return (tcrt_ctype(c) & tcrt_ctype_flag_upper);
}

///////////////////////////////////////////////////////////////////////////////
TCRT_END_EXTERN_C

// EOF ////////////////////////////////////////////////////////////////////////
#endif /* TCRT_CTYPE__H_ */
