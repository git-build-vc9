/*=============================================================================
	stdlib.cpp : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#include <stdlib.h>
#include <signal.h>
#include <errno.h>
#include <fcntl.h>
#include "utils.h"

///////////////////////////////////////////////////////////////////////////////
extern "C" { HANDLE g_process_heap; }

///////////////////////////////////////////////////////////////////////////////
extern "C" TCRT_IMPORT __int64 _strtoi64(const char* str, char** endptr, int base);
extern "C" TCRT_IMPORT unsigned __int64 _strtoui64(const char* str, char** endptr, int base);

///////////////////////////////////////////////////////////////////////////////
char* mktemp(char* templ)
{
	__debugbreak();
	errno = ENOSYS;
	return NULL;
}

extern "C" TCRT_IMPORT char* _mktemp(char* tmp);

int mkstemp(char* templ)
{
	char *filename;

	filename = _mktemp(templ);
	if (filename == NULL)
		return -1;
	return open(filename, O_RDWR | O_CREAT, 0600);
}

long long strtoll(const char* str, char** endptr, int base)
{
	return _strtoi64(str, endptr, base);
}

unsigned long long strtoull(const char* str, char** endptr, int base)
{
	return _strtoui64(str, endptr, base);
}

void* operator new(size_t size)
{
	size = size ? size : 1;

	return HeapAlloc(g_process_heap, 0, size);
}

void operator delete(void* ptr)
{
	if (ptr)
		HeapFree(g_process_heap, 0, ptr);
}

void* malloc(size_t size)
{
	size = size ? size : 1;

	return HeapAlloc(g_process_heap, 0, size);
}

void* calloc(size_t num, size_t size)
{
	size = num * size;
	size = size ? size : 1;

	return HeapAlloc(g_process_heap, HEAP_ZERO_MEMORY, size);
}

void* realloc(void* ptr, size_t size)
{
	size = size ? size : 1;

	if (ptr)
		return HeapReAlloc(g_process_heap, 0, ptr, size);
	return HeapAlloc(g_process_heap, 0, size);
}

void free(void* ptr)
{
	if (ptr)
		HeapFree(g_process_heap, 0, ptr);
}

int system(const char* cmd)
{
	__debugbreak();
	errno = ENOSYS;
	return -1;
}

// EOF ////////////////////////////////////////////////////////////////////////
