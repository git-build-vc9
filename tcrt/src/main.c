/*=============================================================================
	main.c :

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <windows.h>
#include "init.h"

///////////////////////////////////////////////////////////////////////////////
static unsigned int get_command_line(char** dst)
{
	wchar_t* cmd = GetCommandLineW();
	int len;

	len = WideCharToMultiByte(CP_UTF8, 0, cmd, -1, NULL, 0, NULL, NULL);
	*dst = malloc(len);
	return WideCharToMultiByte(CP_UTF8, 0, cmd, -1, *dst, len, NULL, NULL);
}

static char** init_argv(int* argc)
{
	unsigned int quote;
	unsigned int bslash;
	unsigned int argn;
	unsigned int len;
	unsigned int i;
	char** argv;
	char* cmd;

	len = get_command_line(&cmd);
	/**
	 * - 2n backslashes followed by a quotation mark produce n backslashes
	 *   followed by a quotation mark.
	 * - (2n) + 1 backslashes followed by a quotation mark again produce n
	 *   backslashes followed by a quotation mark.
	 * - n backslashes not followed by a quotation mark simply produce n
	 *   backslashes.
	 */
	bslash = 0;
	quote = 0;
	argn = 0;
	for (i = 0; i < len; ++i) {
		if (cmd[i] == '\\') {
			bslash += 1;
			continue;

		} else if (cmd[i] == '\"') {
			if (bslash > 1) {
				bslash -= (bslash / 2);
				memmove(&cmd[i - bslash], &cmd[i], len - i);
				len -= bslash;
				i -= bslash;

			} else if (quote) {
				cmd[quote - 1] = '\0';
				cmd[i] = '\0';
				quote = 0;
				if (cmd[i - 1] != '\0') {
					argn += 1;
				}

			} else {
				quote = i + 1;
			}
		} else if (cmd[i] == ' ' && !quote) {
			cmd[i] = '\0';
			if (i && cmd[i - 1] != '\0')
				argn += 1;

		} else if (cmd[i] == '\0') {
			if (i && cmd[i - 1] != '\0')
				argn += 1;
		}
		bslash = 0;
	}

	argv = realloc(cmd, ((argn + 1) * sizeof(char*)) + i);
	cmd = memmove(argv + argn, argv, len);
	for (i = 0; i < argn; ++i) {
		do {
			len = strlen(cmd);
			argv[i] = cmd;
			cmd += len + 1;
		} while (!len);
	}
	argv[i] = cmd - 1;

	*argc = argn;
	return argv;
}

static char** init_env()
{
	return NULL;
}

///////////////////////////////////////////////////////////////////////////////
void mainCRTStartup(void)
{
	char **argv;
	char **env;
	int mainret;
	int argc;

	g_process_heap = GetProcessHeap();
	run_ctors();
	argv = init_argv(&argc);
	env = init_env();
	mainret = main(argc, argv, env);
	free(argv);
	free(env);
	exit(mainret);
}

void abort(void)
{
	if (g_tcrt_sigaction_slot[SIGABRT].sa_handler != SIG_DFL)
		g_tcrt_sigaction_slot[SIGABRT].sa_handler(SIGABRT);

	for(;;) {
		TerminateProcess(GetCurrentProcess(), EXIT_FAILURE);
		__debugbreak();
	}
}

void exit(int status)
{
	run_dtors();
	ExitProcess(status);
}

#if _M_IX86
	long _InterlockedCompareExchange(long volatile*, long, long);
#elif _M_X64
	__int64 _InterlockedCompareExchange64(__int64 volatile*, __int64, __int64);
#endif

static atexit_entry* atomic_cmpxchg(atexit_entry** dst, atexit_entry* value, atexit_entry* cmp)
{
#if _M_IX86
	return (atexit_entry*) _InterlockedCompareExchange((long volatile*) dst, (long) value, (long) cmp);
#elif _M_X64
	return (atexit_entry*) _InterlockedCompareExchange((__int64 volatile*) dst, (__int64) value, (__int64) cmp);
#endif
}

int atexit(void (*function)(void))
{
	atexit_entry* old;
	atexit_entry* entry;

	entry = (atexit_entry*) malloc(sizeof(atexit_entry));
	if (entry == NULL) {
		errno = ENOMEM;
		return -1;
	}

	entry->fn = function;
	do {
		old = g_atexit_list;
		entry->next = old;
	} while (atomic_cmpxchg(&g_atexit_list, entry, old) != old);

	return 0;
}

void _purecall(void)
{
	__debugbreak();
	abort();
}

// EOF ////////////////////////////////////////////////////////////////////////
