/*=============================================================================
	errno.c : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#include <windows.h>
#include <errno.h>

///////////////////////////////////////////////////////////////////////////////
TCRT_THREAD_LOCAL int errno;

///////////////////////////////////////////////////////////////////////////////
static int get_last_error_win32(void)
{
	DWORD error = GetLastError();

	switch (error) {
	case ERROR_PATH_NOT_FOUND:
	case ERROR_FILE_NOT_FOUND:
	case ERROR_INVALID_NAME:
		return ENOENT;
	case ERROR_INVALID_HANDLE:
		return EBADF;
	case ERROR_ACCESS_DENIED:
	case ERROR_SHARING_VIOLATION:
	case ERROR_LOCK_VIOLATION:
	case ERROR_SHARING_BUFFER_EXCEEDED:
		return EACCES;
	case ERROR_BUFFER_OVERFLOW:
		return ERANGE;
	case ERROR_NOT_ENOUGH_MEMORY:
		return ENOMEM;
	case ERROR_FILE_EXISTS:
	case ERROR_ALREADY_EXISTS:
		return EEXIST;
	default:
		__debugbreak();
	}
	return error | 0x40000000;
}

void tcrt_set_errno_win32(void)
{
	errno = get_last_error_win32();
}

// EOF ////////////////////////////////////////////////////////////////////////
