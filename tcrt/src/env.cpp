/*=============================================================================
	env.cpp : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#include <stdlib.h>
#include "utils.h"

///////////////////////////////////////////////////////////////////////////////
namespace {
	class enviromental {
	public:
		enviromental();
		~enviromental();

		void unset(const char* name);
		char* set(const char* name, const char* value);
		char* get(const char* name) const;

	private:
		struct entry {
			char *name;
			char *value;
		};


		entry*	_entries;
		uint	_count;
	};

	enviromental::enviromental() : _entries(NULL), _count(0)
	{
	}

	enviromental::~enviromental()
	{
		for (uint i = 0; i < _count; ++i) {
			free(_entries[i].name);
			free(_entries[i].value);
		}
		free(_entries);
	}

	char* enviromental::set(const char* name, const char* value)
	{
		char* nam = strdup(name);
		if (name == NULL) {
			errno = ENOMEM;
			return NULL;
		}
		char* val = strdup(value);
		if (value == NULL) {
			free(nam);
			errno = ENOMEM;
			return NULL;
		}

		for (uint i = 0; i < _count; ++i) {
			if (!strcmp(_entries[i].name, name)) {
				free(_entries[i].name);
				free(_entries[i].value);
				_entries[i].name = nam;
				_entries[i].value = val;
				return val;
			}
		}

		entry* p = (entry*) realloc(_entries, (_count + 1) * sizeof(entry));
		if (p == NULL) {
			free(nam);
			free(val);
			errno = ENOMEM;
			return NULL;
		}
		_entries = p;
		_entries[_count].name = nam;
		_entries[_count].value = val;
		_count += 1;
		return val;
	}

	void enviromental::unset(const char* name)
	{
		for (uint i = 0; i < _count; ++i) {
			if (!strcmp(_entries[i].name, name)) {
				free(_entries[i].name);
				free(_entries[i].value);
				i += 1;
				memmove(_entries + i - 1, _entries + i, _count - i);
				_count -= 1;
				break;
			}
		}
	}

	char* enviromental::get(const char* name) const
	{
		for (uint i = 0; i < _count; ++i) {
			if (!strcmp(_entries[i].name, name))
				return _entries[i].value;
		}
		return NULL;
	}

	enviromental	env;
	tcrt::rolock	env_lock;
}

///////////////////////////////////////////////////////////////////////////////
int setenv(const char* envname, const char* envval, int overwrite)
{
	tcrt::buffer<wchar_t> wnam;
	tcrt::buffer<wchar_t> wenv;
	size_t namlen;
	size_t envlen;

	namlen = strlen(envname) + 1;
	envlen = strlen(envval) + 1;
	if (!wnam.resize(namlen * sizeof(wchar_t)))
		return -1;
	if (!wenv.resize(envlen * sizeof(wchar_t)))
		return -1;

	tcrt_utf8_to_utf16(envname, namlen, wnam.get(), namlen);
	tcrt_utf8_to_utf16(envval, envlen, wenv.get(), envlen);
	if (!overwrite && GetEnvironmentVariable(wnam.get(), NULL, 0)) {
		errno = EEXIST;
		return -1;
	}
	if (!SetEnvironmentVariable(wnam.get(), wenv.get())) {
		tcrt_set_errno_win32();
		return -1;
	}

	tcrt::auto_rolock al(env_lock);
	return (env.set(envname, envval) == NULL) ? -1 : 0;
}

int unsetenv(const char* name)
{
	tcrt::buffer<wchar_t> wnam;
	size_t len;

	len = strlen(name) + 1;
	if (!wnam.resize(len * sizeof(wchar_t)))
		return -1;

	tcrt_utf8_to_utf16(name, len, wnam.get(), len);
	if (!SetEnvironmentVariable(wnam.get(), NULL)) {
		tcrt_set_errno_win32();
		return -1;
	}

	tcrt::auto_rolock al(env_lock);
	env.unset(name);
	return 0;
}

char* getenv(const char* name)
{
	tcrt::buffer<wchar_t> wname;
	tcrt::buffer<wchar_t> wenv;
	tcrt::buffer<char> value;
	size_t len;
	char* res;

	tcrt::auto_rolock al(env_lock);
	res = env.get(name);
	if (res != NULL)
		return res;

	len = strlen(name) + 1;
	if (!wname.resize(len * sizeof(wchar_t)))
		return NULL;

	tcrt_utf8_to_utf16(name, len, wname.get(), len);
	len = GetEnvironmentVariableW(wname.get(), NULL, 0);
	if (len) {
		if (!wenv.resize(len * sizeof(wchar_t)))
			return NULL;

		GetEnvironmentVariableW(wname.get(), wenv.get(), len);
		if (!value.resize(len * sizeof(wchar_t)))
			return NULL;

		tcrt_utf16_to_utf8(wenv.get(), len, value.get(), len * sizeof(wchar_t));
		return env.set(name, value.get());
	}
	return NULL;
}

// EOF ////////////////////////////////////////////////////////////////////////
