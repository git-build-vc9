/*=============================================================================
	time.cpp : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

///////////////////////////////////////////////////////////////////////////////
#include <time.h>
#include <sys/time.h>
#include <errno.h>
#include <signal.h>
#include "utils.h"

///////////////////////////////////////////////////////////////////////////////
extern "C" TCRT_IMPORT struct tm* _localtime64(const time_t* timer);
extern "C" TCRT_IMPORT struct tm* _gmtime64(const time_t* timer);

///////////////////////////////////////////////////////////////////////////////
time_t mktime(struct tm* timeptr)
{
	__debugbreak();
	errno = ENOSYS;
	return -1;
}

time_t time(time_t* timer)
{
	FILETIME ftime;
	time_t tmp;

	GetSystemTimeAsFileTime(&ftime);
	tmp = filetime_to_time_t(&ftime);
	if (timer != NULL)
		*timer = tmp;
	return tmp;
}

char* ctime(const time_t* timer)
{
	__debugbreak();
	errno = ENOSYS;
	return NULL;
}

struct tm* gmtime(const time_t* timer)
{
	return _gmtime64(timer);
}

struct tm* localtime(const time_t* timer)
{
	return _localtime64(timer);
}

size_t strftime(char* ptr, size_t maxsize, const char* format, const struct tm* timeptr)
{
	__debugbreak();
	errno = ENOSYS;
	return -1;
}

///////////////////////////////////////////////////////////////////////////////
struct tm* localtime_r(const time_t* time, struct tm* result)
{
	memcpy(result, localtime(time), sizeof(struct tm));
	return result;
}

struct tm* gmtime_r(const time_t* time, struct tm* result)
{
	memcpy(result, gmtime(time), sizeof(struct tm));
	return result;
}

int gettimeofday(struct timeval* tp, void*)
{
	LARGE_INTEGER time;
	FILETIME ftime;

	GetSystemTimeAsFileTime(&ftime);
	time.LowPart = ftime.dwLowDateTime;
	time.HighPart = ftime.dwHighDateTime;
	time.QuadPart -= 116444736000000000ll;
	time.QuadPart /= 10;
	tp->tv_sec = truncate_cast<long>(time.QuadPart / 1000000);
	tp->tv_usec = truncate_cast<long>(time.QuadPart % 1000000);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
struct itimer {
	HANDLE		mutex;
	itimerval	timer;
	timeval*	value;
};

static TCRT_THREAD_LOCAL itimer	itimer_real;

static DWORD WINAPI itimer_routine(void* param)
{
	itimer* tm = (itimer*) param;
	DWORD timeout;

	//
	// TODO: check for overflow
	//
	for(;;) {
		timeout = tm->timer.it_value.tv_sec * 1000;
		timeout += tm->timer.it_value.tv_usec / 1000;
		if (!timeout)
			break;
		if (WaitForSingleObject(tm->mutex, timeout) != WAIT_TIMEOUT)
			break;
		if (g_tcrt_sigaction_slot[SIGALRM].sa_handler != SIG_DFL)
			g_tcrt_sigaction_slot[SIGALRM].sa_handler(SIGALRM);

		tm->timer.it_value.tv_sec = tm->timer.it_interval.tv_sec;
		tm->timer.it_value.tv_usec = tm->timer.it_interval.tv_sec;
	}
	CloseHandle(tm->mutex);
	delete tm;
	return 0;
}

int getitimer(int which, struct itimerval* value)
{
	if (which != ITIMER_REAL) {
		__debugbreak();
		errno = ENOSYS;
		return -1;
	}
	__debugbreak();
	memcpy(value, &itimer_real.timer, sizeof(struct itimerval));
	return 0;
}

int setitimer(int which, const struct itimerval* value, struct itimerval* ovalue)
{
	HANDLE hthread;
	itimer* tm;

	if (which != ITIMER_REAL) {
		__debugbreak();
		errno = ENOSYS;
		return -1;
	}

	if (ovalue != NULL)
		memcpy(ovalue, &itimer_real.timer, sizeof(struct itimerval));

	if (itimer_real.mutex)
		ReleaseMutex(itimer_real.mutex);
	memcpy(&itimer_real.timer, value, sizeof(struct itimerval));
	if (!itimer_real.timer.it_value.tv_sec && !itimer_real.timer.it_value.tv_usec)
		return 0;

	itimer_real.mutex = CreateMutexW(NULL, TRUE, NULL);
	if (itimer_real.mutex == NULL) {
		tcrt_set_errno_win32();
		return -1;
	}
	tm = new itimer;
	if (tm == NULL) {
		errno = ENOMEM;
		return -1;
	}
	tm->mutex = itimer_real.mutex;
	memcpy(&tm->timer, &itimer_real.timer, sizeof(struct itimerval));
	tm->value = &itimer_real.timer.it_value;
	hthread = CreateThread(NULL, 0, itimer_routine, tm, 0, NULL);
	if (hthread == NULL) {
		delete tm;
		tcrt_set_errno_win32();
		return -1;
	}
	CloseHandle(hthread);
	return 0;
}

unsigned alarm(unsigned seconds)
{
	struct itimerval tm = { 0 };

	tm.it_value.tv_sec = seconds;
	setitimer(ITIMER_REAL, &tm, NULL);
	return 0;
}

// EOF ////////////////////////////////////////////////////////////////////////