/*=============================================================================
	ws2_32.h : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#ifndef TINYCRT_SOCKET__H_
#define TINYCRT_SOCKET__H_

///////////////////////////////////////////////////////////////////////////////
#include <sys/socket.h>
#include <sys/select.h>
#include <stdio.h>
#include <errno.h>
#include "utils.h"

///////////////////////////////////////////////////////////////////////////////
#define WSADESCRIPTION_LEN	256
#define WSASYS_STATUS_LEN	128
#define MAX_PROTOCOL_CHAIN	7
#define WSAPROTOCOL_LEN		255

typedef struct WSAData { 
	WORD wVersion;  
	WORD wHighVersion; 
	char szDescription[WSADESCRIPTION_LEN+1]; 
	char szSystemStatus[WSASYS_STATUS_LEN+1]; 
	unsigned short iMaxSockets; 
	unsigned short iMaxUdpDg; 
	char* lpVendorInfo;
} WSADATA,  *LPWSADATA;

typedef struct _WSAPROTOCOLCHAIN {
	int ChainLen;
	DWORD ChainEntries[MAX_PROTOCOL_CHAIN];
} WSAPROTOCOLCHAIN, *LPWSAPROTOCOLCHAIN;

typedef struct _WSAPROTOCOL_INFOW {
    DWORD dwServiceFlags1;
    DWORD dwServiceFlags2;
    DWORD dwServiceFlags3;
    DWORD dwServiceFlags4;
    DWORD dwProviderFlags;
    GUID ProviderId;
    DWORD dwCatalogEntryId;
    WSAPROTOCOLCHAIN ProtocolChain;
    int iVersion;
    int iAddressFamily;
    int iMaxSockAddr;
    int iMinSockAddr;
    int iSocketType;
    int iProtocol;
    int iProtocolMaxOffset;
    int iNetworkByteOrder;
    int iSecurityScheme;
    DWORD dwMessageSize;
    DWORD dwProviderReserved;
    WCHAR szProtocol[WSAPROTOCOL_LEN+1];
} WSAPROTOCOL_INFOW, *LPWSAPROTOCOL_INFOW;

typedef int (WINAPI *WS2STARTUP)(WORD versionRequested, LPWSADATA SAData);
typedef HANDLE (WINAPI *WS2SOCKET)(int af, int type, int protocol, LPWSAPROTOCOL_INFOW protocolInfo, void*, DWORD flags);
typedef int (WINAPI *WS2DUPLICATE)(HANDLE sock, DWORD processId, LPWSAPROTOCOL_INFOW protocolInfo);
typedef int (WINAPI *WS2GETLASTERROR)(void);
typedef int (WINAPI *WS2CLOSE)(HANDLE sock);
typedef HANDLE (WINAPI *WS2ACCEPT)(HANDLE sock, struct sockaddr* addr, int* addr_len);
typedef int (WINAPI *WS2BIND)(HANDLE sock, const struct sockaddr* addr, int addr_len);
typedef int (WINAPI *WS2CONNECT)(HANDLE sock, const struct sockaddr* addr, int addr_len);
typedef int (WINAPI *WS2LISTEN)(HANDLE sock, int backlog);
typedef int (WINAPI *WS2RECV)(HANDLE sock, void* buf, int len, int flags);
typedef int (WINAPI *WS2RECVFROM)(HANDLE sock, void* buf, int len, int flags, struct sockaddr* addr, int* addr_len);
typedef int (WINAPI *WS2SEND)(HANDLE sock, const void* buf, int len, int flags);
typedef int (WINAPI *WS2SENDTO)(HANDLE sock, const void* buf, int len, int flags, const struct sockaddr* addr, int addr_len);
typedef int (WINAPI *WS2GETADDRINFOW)(PCWSTR name, PCWSTR service, const struct addrinfo* hints, struct addrinfo** res);
typedef void (WINAPI *WS2FREEADDRINFOW)(struct addrinfo* ai);
typedef int (WINAPI *WS2GETHOSTNAME)(char* name, int len);
typedef unsigned long (WINAPI *WS2INETADDR)(const char* str);
typedef char* (WINAPI * WS2INETNTOA)(struct in_addr in);
typedef struct hostent* (WINAPI *WS2GETHOSTBYNAME)(const char* str);
typedef int (WINAPI *WS2SELECT)(int nfds, fd_set* readfds, fd_set* writefds, fd_set* exceptfds, const struct timeval* timeout);
typedef int (WINAPI *WS2IOCTL)(HANDLE sock, long cmd, unsigned long* argp);
typedef int (WINAPI *WS2SETSOCKETOPTION)(HANDLE sock, int level, int option, const void* value, int len);
typedef int (WINAPI *WS2FDISSET)(HANDLE sock, fd_set* fs);

///////////////////////////////////////////////////////////////////////////////
extern WS2SOCKET		Ws2_Socket;
extern WS2DUPLICATE		Ws2_Duplicate;
extern WS2GETLASTERROR		Ws2_GetLastError;
extern WS2CLOSE			Ws2_Close;
extern WS2ACCEPT		Ws2_Accept;
extern WS2BIND			Ws2_Bind;
extern WS2CONNECT		Ws2_Connect;
extern WS2LISTEN		Ws2_Listen;
extern WS2RECV			Ws2_Recv;
extern WS2RECVFROM		Ws2_RecvFrom;
extern WS2SEND			Ws2_Send;
extern WS2SENDTO		Ws2_SendTo;
extern WS2GETADDRINFOW		Ws2_GetAddressInfo;
extern WS2FREEADDRINFOW		Ws2_FreeAddressInfo;
extern WS2GETHOSTNAME		Ws2_GetHostName;
extern WS2INETADDR		Ws2_InetAddr;
extern WS2GETHOSTBYNAME		Ws2_GetHostByName;
extern WS2SELECT		Ws2_Select;
extern WS2INETNTOA		Ws2_InetNtoa;
extern WS2IOCTL			Ws2_Ioctl;
extern WS2SETSOCKETOPTION	Ws2_SetSocketOptions;
extern WS2FDISSET		Ws2_FDIsSet;

// EOF ////////////////////////////////////////////////////////////////////////
#endif /* TINYCRT_SOCKET__H_ */
