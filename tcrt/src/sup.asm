;==================================================================================================
;	sup.asm : compiler support
;==================================================================================================

global	__tls_array
global	__fltused
global	__ftol2
global	__ftol2_sse

__tls_array	equ 0x2C
__fltused	equ 0x9876

__ftol2:
__ftol2_sse:
	sub	esp, 4
	fistp	dword [esp]
	mov	eax, [esp]
	add	esp, 4
	ret
