/*=============================================================================
	mmap.cpp : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#include <sys/mman.h>
#include "utils.h"

///////////////////////////////////////////////////////////////////////////////
void* mmap(void* start, size_t length, int prot, int flags, int fd, off_t offset)
{
	HANDLE section;
	HANDLE file;
	DWORD sprot;
	void* map;

	assert(flags == MAP_PRIVATE);

	if (prot & PROT_WRITE)
		sprot = PAGE_READWRITE;
	else if (prot & PROT_READ)
		sprot = PAGE_READONLY;
	else
		sprot = 0;

	file = tcrt::fildes_to_handle(fd);
	section = CreateFileMappingW(file, NULL, sprot, 0, 0, NULL);
	if (!section && GetLastError() == ERROR_ACCESS_DENIED && sprot == PAGE_READWRITE) {
		sprot = PAGE_WRITECOPY;
		prot = FILE_MAP_COPY;
		section = CreateFileMappingW(file, NULL, sprot, 0, 0, NULL);
	}
	if (!section) {
		tcrt_set_errno_win32();
		return MAP_FAILED;
	}
	map = MapViewOfFileEx(	section,
				prot,
				((LARGE_INTEGER*) &offset)->HighPart,
				((LARGE_INTEGER*) &offset)->LowPart,
				length,
				start);
	CloseHandle(section);
	if (!map) {
		tcrt_set_errno_win32();
		return MAP_FAILED;
	}
	return map;
}

int munmap(void* start, size_t /*length*/)
{
	if (!UnmapViewOfFile(start)) {
		tcrt_set_errno_win32();
		return -1;
	}
	return 0;
}

// EOF ////////////////////////////////////////////////////////////////////////
