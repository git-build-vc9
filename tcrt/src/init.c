/*=============================================================================
	init.c : tinycrt initialization	(tiny crt)

	DO NOT use ltcg optimizations in this file under penalty of death
	by TLS corruption.
=============================================================================*/

#include <signal.h>
#include <windows.h>
#include "init.h"

///////////////////////////////////////////////////////////////////////////////
#pragma comment(linker, "/merge:.CRT=.rdata")
#pragma comment(linker, "/defaultlib:kernel32.lib")
#pragma comment(linker, "/disallowlib:libc.lib")
#pragma comment(linker, "/disallowlib:libcd.lib")
#pragma comment(linker, "/disallowlib:libcmt.lib")
#pragma comment(linker, "/disallowlib:libcmtd.lib")
#pragma comment(linker, "/disallowlib:msvcrt.lib")
#pragma comment(linker, "/disallowlib:msvcrtd.lib")

#if defined (_WIN64) && defined (_M_IA64)
#	pragma section(".base", long, read)
	_CRTALLOC(".base")
#endif

_CRTALLOC(".CRT$XIA") _PIFV __xi_a[] = { NULL };
_CRTALLOC(".CRT$XIZ") _PIFV __xi_z[] = { NULL };
_CRTALLOC(".CRT$XCA") _PVFV __xc_a[] = { NULL };
_CRTALLOC(".CRT$XCZ") _PVFV __xc_z[] = { NULL };
_CRTALLOC(".CRT$XPA") _PVFV __xp_a[] = { NULL };
_CRTALLOC(".CRT$XPZ") _PVFV __xp_z[] = { NULL };
_CRTALLOC(".CRT$XTA") _PVFV __xt_a[] = { NULL };
_CRTALLOC(".CRT$XTZ") _PVFV __xt_z[] = { NULL };

///////////////////////////////////////////////////////////////////////////////
static BOOL WINAPI __dyn_tls_dtor(HANDLE handle, DWORD reason, LPVOID reserved);
static BOOL WINAPI __dyn_tls_init(HANDLE handle, DWORD reason, LPVOID reserved);

///////////////////////////////////////////////////////////////////////////////
ULONG _tls_index = 0;

#pragma data_seg(".tls")
#if defined (_M_IA64) || defined (_M_AMD64)
	_CRTALLOC(".tls")
#endif

char _tls_start = 0;

#pragma data_seg(".tls$ZZZ")
#if defined (_M_IA64) || defined (_M_AMD64)
	_CRTALLOC(".tls$ZZZ")
#endif

char _tls_end = 0;

#pragma data_seg()

_CRTALLOC(".CRT$XLA") PIMAGE_TLS_CALLBACK __xl_a = 0;
_CRTALLOC(".CRT$XLZ") PIMAGE_TLS_CALLBACK __xl_z = 0;

#ifdef _WIN64
	_CRTALLOC(".rdata$T") const IMAGE_TLS_DIRECTORY64 _tls_used = {
		(ULONGLONG) &_tls_start,        // start of tls data
		(ULONGLONG) &_tls_end,          // end of tls data
		(ULONGLONG) &_tls_index,        // address of tls_index
		(ULONGLONG) (&__xl_a+1),        // pointer to call back array
		(ULONG) 0,                      // size of tls zero fill
		(ULONG) 0                       // characteristics
	};
#else
	_CRTALLOC(".rdata$T") const IMAGE_TLS_DIRECTORY _tls_used = {
		(ULONG)(ULONG_PTR) &_tls_start, // start of tls data
		(ULONG)(ULONG_PTR) &_tls_end,   // end of tls data
		(ULONG)(ULONG_PTR) &_tls_index, // address of tls_index
		(ULONG)(ULONG_PTR) (&__xl_a+1), // pointer to call back array
		(ULONG) 0,                      // size of tls zero fill
		(ULONG) 0                       // characteristics
	};
#endif

static _CRTALLOC(".CRT$XDA") _PVFV __xd_a = 0;
static _CRTALLOC(".CRT$XDZ") _PVFV __xd_z = 0;

static _CRTALLOC(".CRT$XLC") PIMAGE_TLS_CALLBACK __xl_c = __dyn_tls_init;
static _CRTALLOC(".CRT$XLD") PIMAGE_TLS_CALLBACK __xl_d = __dyn_tls_dtor;

#define FUNCS_PER_NODE  30

typedef struct TlsDtorNode {
    int count;
    struct TlsDtorNode *next;
    _PVFV funcs[FUNCS_PER_NODE];
} TlsDtorNode;

static __declspec(thread) TlsDtorNode*	dtor_list;
static __declspec(thread) TlsDtorNode	dtor_list_head;

atexit_entry* g_atexit_list;

///////////////////////////////////////////////////////////////////////////////
static BOOL WINAPI __dyn_tls_init(HANDLE handle, DWORD reason, LPVOID reserved)
{
	_PVFV *pfunc;

	if (reason != DLL_THREAD_ATTACH) {
		return TRUE;
	}

	#pragma warning(push)
	#pragma warning(disable:26000)
	for(pfunc = &__xd_a + 1; pfunc != &__xd_z; ++pfunc) {
		if (*pfunc != NULL) {
			(*pfunc)();
		}
	}
	#pragma warning(pop)

	return TRUE;
}

int __cdecl __tlregdtor(_PVFV func)
{
	if (dtor_list == NULL) {
		dtor_list = &dtor_list_head;
		dtor_list_head.count = 0;
	}
	else if (dtor_list->count == FUNCS_PER_NODE) {
		TlsDtorNode* pnode = (TlsDtorNode*) malloc(sizeof(TlsDtorNode));
		if (pnode == NULL)
			return -1;
		pnode->count = 0;
		pnode->next = dtor_list;
		dtor_list = pnode;
		dtor_list->count = 0;
	}
	dtor_list->funcs[dtor_list->count++] = func;
	return 0;
}

static BOOL WINAPI __dyn_tls_dtor(HANDLE handle, DWORD reason, LPVOID reserved)
{
	TlsDtorNode *pnode, *pnext;
	int i;

	if (reason != DLL_THREAD_DETACH && reason != DLL_PROCESS_DETACH) {
		return TRUE;
	}

	for (pnode = dtor_list; pnode != NULL; pnode = pnext) {
		for (i = pnode->count - 1; i >= 0; --i) {
			if (pnode->funcs[i] != NULL) {
				(*pnode->funcs[i])();
			}
		}
		pnext = pnode->next;
		if (pnext != NULL) {
			free(pnode);
		}
	}

	return TRUE;
}

static void __atexit(void)
{
	atexit_entry* next = g_atexit_list;

	while (next != NULL) {
		next->fn();
		next = next->next;
	}
	if (g_tcrt_sigaction_slot[SIGTERM].sa_handler != SIG_DFL)
		g_tcrt_sigaction_slot[SIGTERM].sa_handler(SIGTERM);
}

///////////////////////////////////////////////////////////////////////////////
void run_ctors(void)
{
	_PIFV* ii;
	_PVFV* ic;
	int ret;

	__dyn_tls_init(NULL, DLL_THREAD_ATTACH, NULL);
	for(ii = __xi_a; ii < __xi_z; ii++) {
		if( *ii != NULL ) {
			ret = (**ii)();
			if( ret != 0 ) {
				ExitProcess(ret);
			}
		}
	}

	for(ic = __xc_a; ic < __xc_z; ic++) {
		if( *ic != NULL ) {
			(**ic)();
		}
	}
}

void run_dtors(void)
{
	_PVFV* ip;
	_PVFV* it;

	for(ip = __xp_a; ip < __xp_z; ip++) {
		if( *ip != NULL ) {
			(**ip)();
		}
	}

	for(it = __xt_a; it < __xt_z; it++) {
		if( *it != NULL ) {
			(**it)();
		}
	}

	__atexit();
}

// EOF ////////////////////////////////////////////////////////////////////////
