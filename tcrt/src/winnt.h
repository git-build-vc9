/*=============================================================================
	winnt.h : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#include <tcrt.h>
#include <ntstatus.h>
#define WIN32_LEAN_AND_MEAN
#define WIN32_NO_STATUS
#include <windows.h>

///////////////////////////////////////////////////////////////////////////////
TCRT_BEGIN_EXTERN_C

///////////////////////////////////////////////////////////////////////////////
#define NTSUCCESS(x) ((x) < 0x40000000)

///////////////////////////////////////////////////////////////////////////////
typedef ULONG NTSTATUS;

typedef struct _IO_STATUS_BLOCK {
	union {
		NTSTATUS Status;
		PVOID Pointer;
	};
	ULONG_PTR Information;
} IO_STATUS_BLOCK, *PIO_STATUS_BLOCK;

///////////////////////////////////////////////////////////////////////////////
typedef NTSTATUS (WINAPI *NT_READ_FILE)(HANDLE FileHandle,
					HANDLE Event,
					void* ApcRoutine,
					void* ApcContext,
					IO_STATUS_BLOCK* IoStatusBlock,
					void* Buffer,
					ULONG Length,
					LARGE_INTEGER* ByteOffset,
					ULONG* Key);

typedef NTSTATUS (WINAPI *NT_WRITE_FILE)(HANDLE FileHandle,
					 HANDLE Event,
					 void* ApcRoutine,
					 void* ApcContext,
					 IO_STATUS_BLOCK* IoStatusBlock,
					 const void* Buffer,
					 ULONG Length,
					 LARGE_INTEGER* ByteOffset,
					 ULONG* Key);

///////////////////////////////////////////////////////////////////////////////
extern NT_READ_FILE	NtReadFile;
extern NT_WRITE_FILE	NtWriteFile;

///////////////////////////////////////////////////////////////////////////////
TCRT_END_EXTERN_C

// EOF ////////////////////////////////////////////////////////////////////////
