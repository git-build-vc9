/*=============================================================================
	unistd.cpp : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include "winnt.h"
#include "utils.h"
#include "Ws2_32.h"

///////////////////////////////////////////////////////////////////////////////
namespace {
	unsigned int get_disposition(int oflags)
	{
		if (oflags & O_CREAT)
			if (oflags & O_EXCL)
				return CREATE_NEW;
			else if (oflags & O_TRUNC)
				return CREATE_ALWAYS;
			else
				return OPEN_ALWAYS;

		else
			if (oflags & O_TRUNC)
				return TRUNCATE_EXISTING;
			else
				return OPEN_EXISTING;
	}

	HANDLE sys_open(const char* filename, int oflags, mode_t mode)
	{
		tcrt::buffer<wchar_t> wname;
		HANDLE handle;
		size_t len;

		DWORD access = 0;
		if (oflags & O_RDONLY)
			access |= GENERIC_READ;
		if (oflags & O_WRONLY)
			access |= GENERIC_WRITE;

		DWORD flags = 0;
		if (oflags & O_SYMLINK)
			flags |= FILE_FLAG_OPEN_REPARSE_POINT;
		if ((oflags & O_CREAT) && !(mode & S_IWUSR))
			flags |= FILE_ATTRIBUTE_READONLY;
		if (!(oflags & O_BINARY))
			flags |= FILE_FLAG_BACKUP_SEMANTICS;
		if (oflags & O_DELETE) {
			access |= DELETE | FILE_WRITE_ATTRIBUTES;
			//flags |= FILE_FLAG_DELETE_ON_CLOSE;
		}

		len = strlen(filename) + 1;
		if (!wname.resize(len))
			return NULL;
		tcrt_utf8_to_utf16(filename, len, wname.get(), len);
		handle = CreateFileW(wname.get(), access,
				     FILE_SHARE_READ | FILE_SHARE_WRITE,
				     NULL, get_disposition(oflags), flags, NULL);
		if (handle == INVALID_HANDLE_VALUE) {
			tcrt_set_errno_win32();
			return NULL;
		}
		LARGE_INTEGER off = { 0 };
		if ((oflags & O_APPEND) && !SetFilePointerEx(handle, off, NULL, FILE_END)) {
			tcrt_set_errno_win32();
			CloseHandle(handle);
			return NULL;
		}

		return handle;
	}

	ssize_t sys_console_read(HANDLE handle, void* buf, size_t bufsize)
	{
		tcrt::buffer<wchar_t> wbuf;
		DWORD rd;

		__debugbreak();
		if (!wbuf.resize(bufsize))
			return -1;

		if (!ReadConsoleW(handle, wbuf.get(), bufsize * sizeof(wchar_t), &rd, NULL)) {
			tcrt_set_errno_win32();
			return -1;

		}
		return tcrt_utf16_to_utf8(wbuf.get(), rd, (char*) buf, bufsize);
	}

	ssize_t sys_console_write(HANDLE handle, const void* buf, size_t bufsize)
	{
		tcrt::buffer<wchar_t> wbuf;
		DWORD written;

		if (!wbuf.resize(bufsize))
			return -1;

		bufsize = tcrt_utf8_to_utf16((char*) buf, bufsize, wbuf.get(), wbuf.length());
		if (!WriteConsoleW(handle, wbuf.get(), truncate_cast<DWORD>(bufsize), &written, NULL)) {
			tcrt_set_errno_win32();
			return -1;
		}
		return written;
	}

	static ssize_t sys_read(HANDLE handle, void *buf, size_t bufsize, off_t* offset)
	{
		IO_STATUS_BLOCK iostatus;
		NTSTATUS ntstatus;

		ntstatus = NtReadFile(	handle,
					NULL,
					NULL,
					NULL,
					&iostatus,
					buf,
					truncate_cast<ULONG>(bufsize),
					(LARGE_INTEGER*) offset,
					NULL);
		if (ntstatus == STATUS_END_OF_FILE) {
			return 0;

		} else if (!NTSUCCESS(ntstatus)) {
			//tcrt_set_errno_winnt(ntstatus);
			__debugbreak();
			return -1;
		}
		return iostatus.Information;
	}

	static ssize_t sys_write(HANDLE handle, const void *buf, size_t bufsize, off_t* offset)
	{
		IO_STATUS_BLOCK iostatus;
		NTSTATUS ntstatus;

		ntstatus = NtWriteFile(	handle,
					NULL,
					NULL,
					NULL,
					&iostatus,
					buf,
					truncate_cast<ULONG>(bufsize),
					(LARGE_INTEGER*) offset,
					NULL);
		if (!NTSUCCESS(ntstatus)) {
			//tcrt_set_errno_winnt(ntstatus);
			__debugbreak();
			return -1;
		}
		return iostatus.Information;
	}

} /* namespace */

int open(const char *filename, int oflags, ...)
{
	HANDLE handle;
	va_list args;
	mode_t mode;

	va_start(args, oflags);
	mode = va_arg(args, int);
	va_end(args);

	if (!strcmp(filename, "/dev/null"))
		filename = "NUL";

	handle = sys_open(filename, oflags, mode);
	if (!handle) {
		return -1;

	} else if ((size_t) handle > FOPEN_MAX) {
		CloseHandle(handle);
		errno = ENFILE;
		return -1;

	} else if ((size_t) handle < 3) {
		__debugbreak();
	}
	return (int) handle;
}

int close(int fildes)
{
	HANDLE handle = tcrt::fildes_to_handle(fildes);

	if (fildes < 3)
		SetStdHandle(STD_INPUT_HANDLE - fildes, NULL);

	if (tcrt::is_socket_fildes(fildes)) {
		if (Ws2_Close(handle)) {
			errno = Ws2_GetLastError();
			return -1;
		}

	} else {
		if (!CloseHandle(handle)) {
			tcrt_set_errno_win32();
			return -1;
		}
	}
	return 0;
}

int fcntl(int fildes, int cmd, ...)
{
	if (cmd == F_GETFD || cmd == F_SETFD)
		return 0;

	errno = ENOSYS;
	return -1;
}

int dup(int fildes)
{
	HANDLE process;
	HANDLE oldh;
	HANDLE newh;
	bool is_sock;

	oldh = tcrt::fildes_to_handle(fildes);
	is_sock = tcrt::is_socket_fildes(fildes);
	if (!is_sock) {
		process = GetCurrentProcess();
		if (!DuplicateHandle(process, oldh, process, &newh, 0, FALSE, DUPLICATE_SAME_ACCESS)) {
			tcrt_set_errno_win32();
			return -1;
		}
	} else {
		WSAPROTOCOL_INFOW pinfo;

		if (Ws2_Duplicate(oldh, GetCurrentProcessId(), &pinfo)) {
			errno = Ws2_GetLastError();
			return -1;
		}
		newh = Ws2_Socket(0, 0, 0, &pinfo, NULL, 0x01);
		if (newh == INVALID_HANDLE_VALUE) {
			errno = Ws2_GetLastError();
			return -1;
		}
	}
	if ((size_t) newh > FOPEN_MAX) {
		if (!is_sock)
			CloseHandle(newh);
		else
			Ws2_Close(newh);
		errno = ENFILE;
		return -1;
	}
	return (int) newh | ((is_sock) ? SOCKET_FILDES : 0);
}

ssize_t read(int fildes, void *buf, size_t bufsize)
{
	HANDLE handle = tcrt::fildes_to_handle(fildes);

	if (tcrt::is_console_handle(handle))
		return sys_console_read(handle, buf, bufsize);

	if (tcrt::is_socket_fildes(fildes))
		return recv(fildes, buf, bufsize, 0);

	return sys_read(handle, buf, bufsize, NULL);
}


ssize_t pread(int fildes, void *buf, size_t bufsize, off_t offset)
{
	HANDLE handle = tcrt::fildes_to_handle(fildes);

	if (tcrt::is_console_handle(handle))
		return sys_console_read(handle, buf, bufsize);

	return sys_read(handle, buf, bufsize, &offset);
}

ssize_t write(int fildes, const void *buf, size_t bufsize)
{
	HANDLE handle = tcrt::fildes_to_handle(fildes);

	if (tcrt::is_console_handle(handle))
		return sys_console_write(handle, buf, bufsize);

	if (tcrt::is_socket_fildes(fildes))
		return send(fildes, buf, bufsize, 0);

	return sys_write(handle, buf, bufsize, NULL);
}

ssize_t pwrite(int fildes, const void *buf, size_t bufsize, off_t offset)
{
	HANDLE handle = tcrt::fildes_to_handle(fildes);

	if (tcrt::is_console_handle(handle))
		return sys_console_write(handle, buf, bufsize);

	return sys_write(tcrt::fildes_to_handle(fildes), buf, bufsize, &offset);
}

int isatty(int fildes)
{
	if (!tcrt::is_console_handle(tcrt::fildes_to_handle(fildes)))
		return 0;

	return 1;
}

int fsync(int fildes)
{
	HANDLE fh = tcrt::fildes_to_handle(fildes);

	if (tcrt::is_console_handle(fh))
		return 0;
	if (!FlushFileBuffers(fh)) {
		tcrt_set_errno_win32();
		return -1;
	}
	return 0;
}

int ftruncate(int fildes, off_t length)
{
	FILE_END_OF_FILE_INFO eof;

	__debugbreak();
	eof.EndOfFile.QuadPart = length;
	if (!SetFileInformationByHandle(tcrt::fildes_to_handle(fildes), FileEndOfFileInfo, &eof, sizeof(eof))) {
		tcrt_set_errno_win32();
		return -1;
	}
	return 0;
}

off_t lseek(int fildes, off_t offset, int whence)
{
	LARGE_INTEGER off;

	TCRT_STATIC_ASSERT(SEEK_SET == FILE_BEGIN);
	TCRT_STATIC_ASSERT(SEEK_CUR == FILE_CURRENT);
	TCRT_STATIC_ASSERT(SEEK_END == FILE_END);

	off.QuadPart = offset;
	if (!SetFilePointerEx(tcrt::fildes_to_handle(fildes), off, &off, whence)) {
		tcrt_set_errno_win32();
		return -1;
	}
	return off.QuadPart;
}

///////////////////////////////////////////////////////////////////////////////
int access(const char* path, int amode)
{
	struct stat st;

	if (	((amode & (R_OK | W_OK | X_OK)) && (amode & F_OK))
		|| (amode & ~(R_OK | W_OK | X_OK | F_OK))
		|| !amode) {
		errno = EINVAL;
		return -1;
	}

	if (stat(path, &st)) {
		if ((errno == ENOENT) && (amode & F_OK))
			errno = EACCES;
		return -1;
	}
	if ((amode & W_OK) && !(st.st_mode & S_IWUSR)) {
		errno = EACCES;
		return -1;
	}

	return 0;
}

int chdir(const char* path)
{
	tcrt::buffer<wchar_t> wpath;

	if (!wpath.resize(strlen(path) + 1))
		return 1;

	tcrt::utf8_to_utf16(path, wpath.length(), wpath.get(), wpath.length());
	if (!SetCurrentDirectoryW(wpath.get())) {
		tcrt_set_errno_win32();
		return -1;
	}
	return 0;
}

int execl(const char* path, const char* arg0, ... /*, (char *)0 */)
{
	__debugbreak();
	errno = ENOSYS;
	return -1;
}

int execlp(const char* file, const char* arg0, ... /*, (char *)0 */)
{
	__debugbreak();
	errno = ENOSYS;
	return -1;
}

pid_t fork(void)
{
	__debugbreak();
	errno = ENOSYS;
	return -1;
}

char* getcwd(char* buf, size_t size)
{
	tcrt::buffer<wchar_t> name;
	DWORD len = size - 1;

	if (!size) {
		errno = EINVAL;
		return NULL;
	}

	if (!name.resize(len))
		return NULL;
	len = GetCurrentDirectoryW(len, name.get());
	if (!len) {
		__debugbreak();
	} else if (len > (size - 1)
		|| !WideCharToMultiByte(CP_UTF8, 0, name.get(), len + 1, buf, size, NULL, NULL)) {
		errno = ERANGE;
		buf = NULL;
	}
	return buf;
}

int getpagesize(void)
{
	SYSTEM_INFO sysinfo;

	GetSystemInfo(&sysinfo);
	return sysinfo.dwAllocationGranularity;
}

pid_t getpid(void)
{
	return GetCurrentProcessId();
}

uid_t getuid(void)
{
	__debugbreak();
	errno = ENOSYS;
	return -1;
}

int pipe(int fildes[2])
{
	HANDLE handle[2];

	if (!CreatePipe(handle + 0, handle + 1, NULL, 0)) {
		tcrt_set_errno_win32();
		return -1;
	}
	if ((size_t) handle[0] > FOPEN_MAX || (size_t) handle[1] > FOPEN_MAX) {
		CloseHandle(handle[0]);
		CloseHandle(handle[1]);
		errno = ENFILE;
		return -1;
	}
	fildes[0] = (int) handle[0];
	fildes[1] = (int) handle[1];
	return 0;
}

int rmdir(const char* path)
{
	tcrt::buffer<wchar_t> wpath;

	if (!wpath.resize(strlen(path) + 1))
		return -1;

	tcrt::utf8_to_utf16(path, wpath.length(), wpath.get(), wpath.length());
	if (!RemoveDirectoryW(wpath.get())) {
		tcrt_set_errno_win32();
		return -1;
	}
	return 0;
}

unsigned sleep(unsigned seconds)
{
	Sleep(seconds * 1000);
	return 0;
}

int symlink(const char *path1, const char *path2)
{
	errno = ENOSYS;
	return -1;
}

int readlink(const char* path, char* buf, size_t bufsize)
{
	errno = ENOSYS;
	return -1;
}

int link(const char* oldpath, const char* newpath)
{
	tcrt::buffer<wchar_t> woldpath;
	tcrt::buffer<wchar_t> wnewpath;

	if (!woldpath.resize(strlen(oldpath) + 1))
		return 1;
	if (!wnewpath.resize(strlen(newpath) + 1))
		return 1;

	tcrt::utf8_to_utf16(oldpath, woldpath.length(), woldpath.get(), woldpath.length());
	tcrt::utf8_to_utf16(newpath, wnewpath.length(), wnewpath.get(), wnewpath.length());
	if (!CreateHardLinkW(wnewpath.get(), woldpath.get(), NULL)) {
		tcrt_set_errno_win32();
		return -1;
	}
	return 0;
}

int unlink(const char* path)
{
	FILE_DISPOSITION_INFO del = { TRUE };
	FILE_BASIC_INFO binfo;
	HANDLE fh;

	fh = sys_open(path, O_BINARY | O_SYMLINK | O_DELETE, 0);
	if (!fh)
		return -1;
	if (!SetFileInformationByHandle(fh, FileDispositionInfo, &del, sizeof(del))) {
		GetFileInformationByHandleEx(fh, FileBasicInfo, &binfo, sizeof(binfo));
		binfo.FileAttributes &= ~FILE_ATTRIBUTE_READONLY;
		SetFileInformationByHandle(fh, FileBasicInfo, &binfo, sizeof(binfo));
		if (!SetFileInformationByHandle(fh, FileDispositionInfo, &del, sizeof(del))) {
			CloseHandle(fh);
			tcrt_set_errno_win32();
			return -1;
		}
	}
	CloseHandle(fh);
	return 0;
}

// EOF ////////////////////////////////////////////////////////////////////////
