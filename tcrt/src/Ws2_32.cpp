/*=============================================================================
	Ws2_32.cpp : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#include "Ws2_32.h"

///////////////////////////////////////////////////////////////////////////////
static HANDLE WINAPI Ws2_Socket_Stub(int af, int type, int protocol, LPWSAPROTOCOL_INFOW protocolInfo, void*, DWORD flags);
static int WINAPI Ws2_Duplicate_Stub(HANDLE sock, DWORD processId, LPWSAPROTOCOL_INFOW protocolInfo);
static int WINAPI Ws2_GetLastError_Stub();
static int WINAPI Ws2_Close_Stub(HANDLE sock);
static HANDLE WINAPI Ws2_Accept_Stub(HANDLE sock, struct sockaddr* addr, int* addr_len);
static int WINAPI Ws2_Bind_Stub(HANDLE sock, const struct sockaddr* addr, int addr_len);
static int WINAPI Ws2_Connect_Stub(HANDLE sock, const struct sockaddr* addr, int addr_len);
static int WINAPI Ws2_Listen_Stub(HANDLE sock, int backlog);
static int WINAPI Ws2_Recv_Stub(HANDLE sock, void* buf, int len, int flags);
static int WINAPI Ws2_RecvFrom_Stub(HANDLE sock, void* buf, int len, int flags, struct sockaddr* addr, int* addr_len);
static int WINAPI Ws2_Send_Stub(HANDLE sock, const void* buf, int len, int flags);
static int WINAPI Ws2_SendTo_Stub(HANDLE sock, const void* buf, int len, int flags, const struct sockaddr* addr, int addr_len);
static int WINAPI Ws2_GetAddressInfo_Stub(PCWSTR name, PCWSTR service, const struct addrinfo* hints, struct addrinfo** res);
static void WINAPI Ws2_FreeAddressInfo_Stub(struct addrinfo* ai);
static int WINAPI Ws2_GetHostName_Stub(char* name, int len);
static unsigned long WINAPI Ws2_InetAddr_Stub(const char* str);
static char* WINAPI Ws2_InetNtoa_Stub(struct in_addr in);
static struct hostent* WINAPI Ws2_GetHostByName_Stub(const char* name);
static int WINAPI Ws2_Select_Stub(int nfds, fd_set* readfds, fd_set* writefds, fd_set* exceptfds, const struct timeval* timeout);
static int WINAPI Ws2_Ioctl_Stub(HANDLE sock, long cmd, unsigned long* argp);
static int WINAPI Ws2_SetSocketOptions_Stub(HANDLE sock, int level, int option, const void* value, int len);
static int WINAPI Ws2_FDIsSet_Stub(HANDLE sock, fd_set* fs);

///////////////////////////////////////////////////////////////////////////////
static HMODULE		Ws2_32;
static WS2STARTUP	Ws2_Startup;
WS2SOCKET		Ws2_Socket = Ws2_Socket_Stub;
WS2DUPLICATE		Ws2_Duplicate = Ws2_Duplicate_Stub;
WS2GETLASTERROR		Ws2_GetLastError = Ws2_GetLastError_Stub;
WS2CLOSE		Ws2_Close = Ws2_Close_Stub;
WS2ACCEPT		Ws2_Accept = Ws2_Accept_Stub;
WS2BIND			Ws2_Bind = Ws2_Bind_Stub;
WS2CONNECT		Ws2_Connect = Ws2_Connect_Stub;
WS2LISTEN		Ws2_Listen = Ws2_Listen_Stub;
WS2RECV			Ws2_Recv = Ws2_Recv_Stub;
WS2RECVFROM		Ws2_RecvFrom = Ws2_RecvFrom_Stub;
WS2SEND			Ws2_Send = Ws2_Send_Stub;
WS2SENDTO		Ws2_SendTo = Ws2_SendTo_Stub;
WS2GETADDRINFOW		Ws2_GetAddressInfo = Ws2_GetAddressInfo_Stub;
WS2FREEADDRINFOW	Ws2_FreeAddressInfo = Ws2_FreeAddressInfo_Stub;
WS2GETHOSTNAME		Ws2_GetHostName = Ws2_GetHostName_Stub;
WS2INETADDR		Ws2_InetAddr = Ws2_InetAddr_Stub;
WS2GETHOSTBYNAME	Ws2_GetHostByName = Ws2_GetHostByName_Stub;
WS2SELECT		Ws2_Select = Ws2_Select_Stub;
WS2INETNTOA		Ws2_InetNtoa = Ws2_InetNtoa_Stub;
WS2IOCTL		Ws2_Ioctl = Ws2_Ioctl_Stub;
WS2SETSOCKETOPTION	Ws2_SetSocketOptions = Ws2_SetSocketOptions_Stub;
WS2FDISSET		Ws2_FDIsSet = Ws2_FDIsSet_Stub;

///////////////////////////////////////////////////////////////////////////////
static void Init_Ws2_32()
{
	static WSADATA WSAData;
	static tcrt::rolock Lock;

	tcrt::auto_rolock al(Lock);
	if (Ws2_32)
		return;

	Ws2_32 = LoadLibraryW(L"Ws2_32.dll");
	Ws2_Startup = (WS2STARTUP) GetProcAddress(Ws2_32, "WSAStartup");

	if (!Ws2_Startup || Ws2_Startup(MAKEWORD(2,2), &WSAData)) {
		fprintf(stderr, "fatal: failed to initialize the Winsock2 library\n");
		abort();
	}
}

template<class T> inline bool get_address(T* fn, const char* name)
{
	Init_Ws2_32();
	if (!(*fn = (T) GetProcAddress(Ws2_32, name))) {
		errno = ENOSYS;
		return false;
	}
	return true;
}

///////////////////////////////////////////////////////////////////////////////
static HANDLE WINAPI Ws2_Socket_Stub(int af, int type, int protocol, LPWSAPROTOCOL_INFOW protocolInfo, void* g, DWORD flags)
{
	if (!get_address(&Ws2_Socket, "WSASocketW"))
		return INVALID_HANDLE_VALUE;
	return Ws2_Socket(af, type, protocol, protocolInfo, g, flags);
}

static int WINAPI Ws2_Duplicate_Stub(HANDLE sock, DWORD processId, LPWSAPROTOCOL_INFOW protocolInfo)
{
	if (!get_address(&Ws2_Duplicate, "WSADuplicateSocketW"))
		return -1;
	return Ws2_Duplicate(sock, processId, protocolInfo);
}

static int WINAPI Ws2_GetLastError_Stub()
{
	if (!get_address(&Ws2_GetLastError, "WSAGetLastError"))
		return -1;
	return Ws2_GetLastError();
}

static int WINAPI Ws2_Close_Stub(HANDLE sock)
{
	if (!get_address(&Ws2_Close, "closesocket"))
		return -1;
	return Ws2_Close(sock);
}

static HANDLE WINAPI Ws2_Accept_Stub(HANDLE sock, struct sockaddr* addr, int* addr_len)
{
	if (!get_address(&Ws2_Accept, "accept"))
		return INVALID_HANDLE_VALUE;
	return Ws2_Accept(sock, addr, addr_len);
}

static int WINAPI Ws2_Bind_Stub(HANDLE sock, const struct sockaddr* addr, int addr_len)
{
	if (!get_address(&Ws2_Bind, "bind"))
		return -1;
	return Ws2_Bind(sock, addr, addr_len);
}

static int WINAPI Ws2_Connect_Stub(HANDLE sock, const struct sockaddr* addr, int addr_len)
{
	if (!get_address(&Ws2_Connect, "connect"))
		return -1;
	return Ws2_Connect(sock, addr, addr_len);
}

static int WINAPI Ws2_Listen_Stub(HANDLE sock, int backlog)
{
	if (!get_address(&Ws2_Listen, "listen"))
		return -1;
	return Ws2_Listen(sock, backlog);
}

static int WINAPI Ws2_Recv_Stub(HANDLE sock, void* buf, int len, int flags)
{
	if (!get_address(&Ws2_Recv, "recv"))
		return -1;
	return Ws2_Recv(sock, buf, len, flags);
}

static int WINAPI Ws2_RecvFrom_Stub(HANDLE sock, void* buf, int len, int flags, struct sockaddr* addr, int* addr_len)
{
	if (!get_address(&Ws2_RecvFrom, "recvfrom"))
		return -1;
	return Ws2_RecvFrom(sock, buf, len, flags, addr, addr_len);
}

static int WINAPI Ws2_Send_Stub(HANDLE sock, const void* buf, int len, int flags)
{
	if (!get_address(&Ws2_Send, "send"))
		return -1;
	return Ws2_Send(sock, buf, len, flags);
}

static int WINAPI Ws2_SendTo_Stub(HANDLE sock, const void* buf, int len, int flags, const struct sockaddr* addr, int addr_len)
{
	if (!get_address(&Ws2_SendTo, "sendto"))
		return -1;
	return Ws2_SendTo(sock, buf, len, flags, addr, addr_len);
}

static int WINAPI Ws2_GetAddressInfo_Stub(PCWSTR name, PCWSTR service, const struct addrinfo* hints, struct addrinfo** res)
{
	if (!get_address(&Ws2_GetAddressInfo, "GetAddrInfoW"))
		return -1;
	return Ws2_GetAddressInfo(name, service, hints, res);
}

static void WINAPI Ws2_FreeAddressInfo_Stub(struct addrinfo* ai)
{
	if (!get_address(&Ws2_FreeAddressInfo, "FreeAddrInfoW"))
		return;
	Ws2_FreeAddressInfo(ai);
}

static int WINAPI Ws2_GetHostName_Stub(char* name, int len)
{
	if (!get_address(&Ws2_GetHostName, "gethostname"))
		return -1;
	return Ws2_GetHostName(name, len);
}

static unsigned long WINAPI Ws2_InetAddr_Stub(const char* str)
{
	if (!get_address(&Ws2_InetAddr, "inet_addr"))
		return -1;
	return Ws2_InetAddr(str);
}

static char* WINAPI Ws2_InetNtoa_Stub(struct in_addr in)
{
	if (!get_address(&Ws2_InetNtoa, "inet_ntoa"))
		return NULL;
	return Ws2_InetNtoa(in);
}

static struct hostent* WINAPI Ws2_GetHostByName_Stub(const char* name)
{
	if (!get_address(&Ws2_GetHostByName, "gethostbyname"))
		return NULL;
	return Ws2_GetHostByName(name);
}

static int WINAPI Ws2_Select_Stub(int nfds, fd_set* readfds, fd_set* writefds, fd_set* exceptfds, const struct timeval* timeout)
{
	if (!get_address(&Ws2_Select, "select"))
		return NULL;
	return Ws2_Select(nfds, readfds, writefds, exceptfds, timeout);
}

static int WINAPI Ws2_Ioctl_Stub(HANDLE sock, long cmd, unsigned long* argp)
{
	if (!get_address(&Ws2_Ioctl, "ioctlsocket")) //0x8004667e SO_NOBLOCK
		return NULL;
	return Ws2_Ioctl(sock, cmd, argp);
}

static int WINAPI Ws2_SetSocketOptions_Stub(HANDLE sock, int level, int option, const void* value, int len)
{
	if (!get_address(&Ws2_SetSocketOptions, "setsockopt"))
		return NULL;
	return Ws2_SetSocketOptions(sock, level, option, value, len);
}

static int WINAPI Ws2_FDIsSet_Stub(HANDLE sock, fd_set* fs)
{
	if (!get_address(&Ws2_FDIsSet, "__WSAFDIsSet"))
		return NULL;
	return Ws2_FDIsSet(sock, fs);
}

// EOF ////////////////////////////////////////////////////////////////////////
