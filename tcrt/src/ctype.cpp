/*=============================================================================
	ctype.cpp : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

///////////////////////////////////////////////////////////////////////////////
#include <ctype.h>
#include <errno.h>

///////////////////////////////////////////////////////////////////////////////
enum {
	UP = tcrt_ctype_flag_upper,
	LO = tcrt_ctype_flag_lower,
	CT = tcrt_ctype_flag_cntrl,
	DG = tcrt_ctype_flag_digit,
	PT = tcrt_ctype_flag_punct,
	HX = tcrt_ctype_flag_xdigit,
	SP = tcrt_ctype_flag_space
};

///////////////////////////////////////////////////////////////////////////////
namespace {
	const unsigned char TypeLookup[128] = {
		CT, CT, CT, CT, CT, CT, CT, CT,	CT,		//0 - 31
		CT|SP, CT|SP, CT|SP, CT|SP, CT|SP,
		CT, CT, CT, CT, CT, CT,	CT, CT, CT,
		CT, CT, CT, CT, CT, CT, CT, CT, CT,

		SP,						//32

		PT, PT, PT, PT, PT, PT, PT, PT,			//33 - 47
		PT, PT, PT, PT, PT, PT, PT,

		DG, DG, DG, DG, DG, DG, DG, DG,			//48 - 57
		DG, DG,

		PT, PT, PT, PT, PT, PT, PT,			//58 - 64

		UP|HX, UP|HX, UP|HX, UP|HX, UP|HX, UP|HX,	//65 - 90
		UP, UP, UP, UP, UP, UP, UP, UP, UP, UP, 
		UP, UP, UP, UP, UP, UP, UP, UP, UP, UP,

		PT, PT, PT, PT, PT, PT,				//91 - 96

		LO|HX, LO|HX, LO|HX, LO|HX, LO|HX, LO|HX,	//97 - 122
		LO, LO, LO, LO, LO, LO, LO, LO, LO, LO,
		LO, LO, LO, LO, LO, LO, LO, LO, LO, LO,

		PT, PT, PT, PT,					//123 - 126

		CT						//127
	};
}

///////////////////////////////////////////////////////////////////////////////
int tolower(int c)
{
	if (isupper(c))
		return c - 'A' + 'a';
	return c;
}

int toupper(int c)
{
	if (islower(c))
		return c - 'a' + 'A';
	return c;
}

int tcrt_ctype(int c)
{
	if (c < 128)
		return TypeLookup[c];

	__debugbreak();
	return 0;
}

// EOF ////////////////////////////////////////////////////////////////////////
