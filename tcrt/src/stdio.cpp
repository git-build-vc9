/*=============================================================================
	stdio.cpp : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include "utils.h"

///////////////////////////////////////////////////////////////////////////////
extern "C" TCRT_IMPORT int _vsnprintf(char* dst, size_t len, const char* format, va_list arg);

///////////////////////////////////////////////////////////////////////////////
struct FILE {
	enum { kBufferSize = 4096 };

	FILE(int fildes);

	bool read();
	size_t read(void* buf, size_t bufsize);
	size_t write(const void* buf, size_t bufsize);

	int			_fildes;
	bool			_error : 1;
	bool			_eof : 1;
	tcrt::rolock		_lock;
	char*			_pos;
	tcrt::buffer<char>	_buf;
	uint			_size;
	off_t			_offset;
};

///////////////////////////////////////////////////////////////////////////////
static FILE stdinput(0);
static FILE stdoutput(1);
static FILE stderror(2);

FILE* const stdin = &stdinput;
FILE* const stdout = &stdoutput;
FILE* const stderr = &stderror;

///////////////////////////////////////////////////////////////////////////////
FILE::FILE(int fildes)
	: _fildes(fildes), _error(false), _eof(false),
	_pos(NULL), _size(0), _offset(0)
{
}

bool FILE::read()
{
	ssize_t rd;

	if (_buf.get() == NULL) {
		if (!_buf.resize(kBufferSize))
			return false;
		_pos = _buf.get();
	}
	rd = ::read(_fildes, _buf.get(), kBufferSize);
	if (rd < 0) {
		_error = true;

	} else if (!rd) {
		_eof = true;

	} else {
		_pos = _buf.get();
		_offset += _size;
		_size = rd;
		return true;
	}
	return false;
}

size_t FILE::read(void* buf, size_t bufsize)
{
	tcrt::auto_rolock al(_lock);
	size_t len;
	size_t rd;

	rd = 0;
	do {
		len = _pos - _buf.get();
		if (len < _size) {
			len = _size - len;
			if (bufsize < len)
				len = bufsize;

			memcpy(buf, _pos, len);
			_pos += len;
			rd += len;
			bufsize -= len;
		} else {
			if (!read())
				break;
		}
	} while (bufsize);

	return rd;
}

size_t FILE::write(const void* buf, size_t bufsize)
{
	tcrt::auto_rolock al(_lock);
	ssize_t res;

	res = ::write(_fildes, buf, bufsize);
	if (res < 0) {
		__debugbreak();
		_error = true;
		return 0;
	}
	_pos = _buf.get();
	_offset += _size + res;
	_size = 0;
	return res;
}

static int get_oflags(const char* mode)
{
	switch (mode[0]) {
	case 'r':
		if (mode[1] == '+' || (mode[1] == 'b' && mode[2] == '+'))
			return O_RDWR | O_BINARY;
		else
			return O_RDONLY | O_BINARY;

	case 'w':
		if (mode[1] == '+' || (mode[1] == 'b' && mode[2] == '+'))
			return (O_RDWR | O_CREAT | O_TRUNC | O_BINARY);
		else
			return (O_WRONLY | O_CREAT | O_TRUNC | O_BINARY);

	case 'a':
		if (mode[1] == '+' || (mode[1] == 'b' && mode[2] == '+'))
			return (O_RDWR | O_CREAT | O_APPEND | O_BINARY);
		else
			return (O_WRONLY | O_CREAT | O_APPEND | O_BINARY);

	default:
		return 0;
	};
}

///////////////////////////////////////////////////////////////////////////////
int dup2(int oldfd, int newfd)
{
	if (newfd < 0)
		errno = EINVAL;
	else if (newfd > 2)
		errno = ENOSYS;
	else {
		HANDLE old;
		int fd;

		fd = dup(oldfd);
		if (fd < 0)
			return -1;
		old = GetStdHandle(STD_INPUT_HANDLE - newfd);
		SetStdHandle(STD_INPUT_HANDLE - newfd, (HANDLE) fd);
		CloseHandle(old);
		return 0;
	}
	__debugbreak();
	return -1;
}

int rename(const char* oldname, const char* newname)
{
	tcrt::buffer<wchar_t> wold;
	tcrt::buffer<wchar_t> wnew;
	size_t oldlen = strlen(oldname) + 1;
	size_t newlen = strlen(newname) + 1;

	if (!wold.resize(oldlen))
		return -1;
	if (!wnew.resize(newlen))
		return -1;
	tcrt_utf8_to_utf16(oldname, oldlen, wold.get(), oldlen);
	tcrt_utf8_to_utf16(newname, newlen, wnew.get(), newlen);
	if (!MoveFileExW(wold.get(), wnew.get(), MOVEFILE_REPLACE_EXISTING | MOVEFILE_COPY_ALLOWED)) {
		tcrt_set_errno_win32();
		return -1;
	}
	return 0;
}

FILE* fopen(const char* filename, const char* mode)
{
	FILE* stream;
	int fd;

	fd = open(filename, get_oflags(mode), 0600);
	if (fd < 0)
		return NULL;
	stream = new FILE(fd);
	if (stream == NULL)
		errno = ENOMEM;
	return stream;
}

FILE* freopen(const char* filename, const char* mode)
{
	__debugbreak();
	errno = ENOSYS;
	return NULL;
}

FILE* fdopen(int fildes, const char* mode)
{
	return new FILE(fildes);
}

int fileno(FILE* stream)
{
	return stream->_fildes;
}

int fflush(FILE* stream)
{
	if (stream == NULL)
		return 0;

	return fsync(stream->_fildes);
}

int fclose(FILE* stream)
{
	int res;

	res = close(stream->_fildes);
	if (stream != stdin && stream != stdout && stream != stderr)
		delete stream;
	return (res < 0) ? EOF : 0;
}

int setvbuf(FILE* stream, char* buffer, int mode, size_t size)
{
	__debugbreak();
	errno = ENOSYS;
	return -1;
}

int sprintf(char* dst, const char* format, ...)
{
	va_list args;
	int res;

	va_start(args, format);
	res = vsprintf(dst, format, args);
	va_end(args);
	return res;
}

int snprintf(char* dst, size_t len, const char* fmt, ...)
{
	va_list args;
	int res;

	va_start(args, fmt);
	res = vsnprintf(dst, len, fmt, args);
	va_end(args);
	return res;
}

int vsnprintf(char* dst, size_t len, const char* fmt, va_list args)
{
	tcrt::buffer<char> buf;
	int ret = -1;

	if (len > 0) {
		ret = _vsnprintf(dst, len, fmt, args);
		if (ret == (len - 1))
			ret = -1;
		dst[len - 1] = '\0';
	}
	if (ret != -1)
		return ret;

	if (len < 128)
		len = 128;

	while (ret == -1) {
		len *= 4;
		if (!buf.resize(len))
			break;
		ret = _vsnprintf(buf.get(), len, fmt, args);
		if (ret == (len - 1))
			ret = -1;
	}
	return ret;
}

int printf(const char* format, ...)
{
	va_list args;
	int res;

	va_start(args, format);
	res = vprintf(format, args);
	va_end(args);
	return res;
}

int vprintf(const char* fmt, va_list args)
{
	return vfprintf(stdout, fmt, args);
}

int fprintf(FILE* stream, const char* fmt, ...)
{
	va_list args;
	int res;

	va_start(args, fmt);
	res = vfprintf(stream, fmt, args);
	va_end(args);
	return res;
}

int vfprintf(FILE* stream, const char* fmt, va_list args)
{
	tcrt::buffer<char> buf;
	size_t len = 32;
	int res = -1;

	do {
		len *= 4;
		if (!buf.resize(len))
			break;
		res = _vsnprintf(buf.get(), len, fmt, args);

	} while (res == -1);

	if (res != -1)
		res = stream->write(buf.get(), res);
	return res;
}

int fgetc(FILE* stream)
{
	size_t res;
	char c;

	res = stream->read(&c, sizeof(c));
	return (!res) ? EOF : c;
}

char* fgets(char* str, int num, FILE* stream)
{
	tcrt::auto_rolock al(stream->_lock);
	size_t len;
	char* dst = str;
	char* end;
	char* p;

	if (stream->_pos == NULL && !stream->read())
		return NULL;

	num -= 1;
	while (num > 0) {
		p = stream->_pos;
		end = stream->_buf.get() + stream->_size;

		while (p != end && *p++ != '\n');
		len = p - stream->_pos;
		if ((uint) num < len)
			len = num;

		memcpy(dst, stream->_pos, len);
		stream->_pos += len;
		dst += len;
		num -= len;

		if (p != end)
			void(0);
		else if (stream->read())
			continue;
		else if (str == dst)
			return NULL;
		*dst = '\0';
		return str;
	}
	return NULL;
}

int fputc(int character, FILE * stream)
{
	char c = (char) character;

	if (!stream->write(&c, sizeof(c)))
		return EOF;
	return character;
}

int fputs(const char* str, FILE* stream)
{
	size_t len = strlen(str);
	char endl = '\n';

	if (len && !stream->write(str, len))
		return EOF;
	return 0;
}

int getc(FILE* stream)
{
	__debugbreak();
	errno = ENOSYS;
	return -1;
}

int putc(int character, FILE* stream)
{
	__debugbreak();
	errno = ENOSYS;
	return -1;
}

int putchar(int character)
{
	char c = (char) character;
	size_t res;

	res = stdout->write(&c, sizeof(c));
	if (!res)
		return EOF;
	return c;
}

int puts(const char* str)
{
	char endl = '\n';
	size_t res;

	res = stdout->write(str, strlen(str));
	if (!res)
		return EOF;
	res = stdout->write(&endl, sizeof(endl));
	if (!res)
		return EOF;
	return 0;
}

int ungetc(int character, FILE* stream)
{
	__debugbreak();
	errno = ENOSYS;
	return -1;
}

size_t fread(void* ptr, size_t size, size_t count, FILE* stream)
{
	if (!size || !count) {
		errno = EINVAL;
		return 0;
	}
	return stream->read(ptr, size * count) / size;
}

size_t fwrite(const void* ptr, size_t size, size_t count, FILE* stream)
{
	if (!size || !count) {
		errno = EINVAL;
		return 0;
	}
	return stream->write(ptr, size * count) / size;
}

int fgetpos(FILE* stream, fpos_t* position)
{
	tcrt::auto_rolock al(stream->_lock);
	*position = stream->_offset + (stream->_pos - stream->_buf.get());
	return 0;
}

int fseek(FILE* stream, long long offset, int origin)
{
	__debugbreak();
	tcrt::auto_rolock al(stream->_lock);
	LARGE_INTEGER fsize;

	GetFileSizeEx(tcrt::fildes_to_handle(stream->_fildes), &fsize);
	switch (origin) {
	case SEEK_SET:
		break;
	case SEEK_CUR:
		offset += stream->_offset + (stream->_pos - stream->_buf.get());
		break;
	case SEEK_END:
		offset += fsize.QuadPart;
		break;
	default:
		errno = EINVAL;
		return -1;
	}
	if (offset < 0 || offset > fsize.QuadPart) {
		errno = EINVAL;
		return -1;
	}
	stream->_offset = offset - (offset % FILE::kBufferSize);
	if (lseek(stream->_fildes, stream->_offset, SEEK_SET) < 0)
		return -1;
	if (!stream->read())
		return -1;
	stream->_pos += (offset % FILE::kBufferSize);
	return 0;
}

int fsetpos(FILE* stream, const fpos_t* pos)
{
	return fseek(stream, *pos, SEEK_SET);
}

long long ftell(FILE* stream)
{
	return stream->_offset + (stream->_pos - stream->_buf.get());
}

void rewind(FILE* stream)
{
	tcrt::auto_rolock al(stream->_lock);
	stream->_offset = 0;
	stream->_pos = stream->_buf.get();
}

void clearerr(FILE* stream)
{
	stream->_error = false;
}

int feof(FILE* stream)
{
	return stream->_eof;
}

int ferror(FILE* stream)
{
	return stream->_error;
}

void perror(const char* str)
{
	__debugbreak();
	errno = ENOSYS;
}

// EOF ////////////////////////////////////////////////////////////////////////
