/*=============================================================================
	winnt.cpp : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#include "winnt.h"

///////////////////////////////////////////////////////////////////////////////
static NTSTATUS WINAPI NtReadFileStub(HANDLE FileHandle,
				      HANDLE Event,
				      void* ApcRoutine,
				      void* ApcContext,
				      IO_STATUS_BLOCK* IoStatusBlock,
				      void* Buffer,
				      ULONG Length,
				      LARGE_INTEGER* ByteOffset,
				      ULONG* Key)
{
	NtReadFile = (NT_READ_FILE) GetProcAddress(GetModuleHandleW(L"ntdll.dll"), "NtReadFile");

	return NtReadFile(	FileHandle,
				Event,
				ApcRoutine,
				ApcContext,
				IoStatusBlock,
				Buffer,
				Length,
				ByteOffset,
				Key);
}

static NTSTATUS WINAPI NtWriteFileStub(HANDLE FileHandle,
				       HANDLE Event,
				       void* ApcRoutine,
				       void* ApcContext,
				       IO_STATUS_BLOCK* IoStatusBlock,
				       const void* Buffer,
				       ULONG Length,
				       LARGE_INTEGER* ByteOffset,
				       ULONG* Key)
{
	NtWriteFile = (NT_WRITE_FILE) GetProcAddress(GetModuleHandleW(L"ntdll.dll"), "NtWriteFile");

	return NtWriteFile(	FileHandle,
				Event,
				ApcRoutine,
				ApcContext,
				IoStatusBlock,
				Buffer,
				Length,
				ByteOffset,
				Key);
}

///////////////////////////////////////////////////////////////////////////////
NT_READ_FILE	NtReadFile	= NtReadFileStub;
NT_WRITE_FILE	NtWriteFile	= NtWriteFileStub;

// EOF ////////////////////////////////////////////////////////////////////////
