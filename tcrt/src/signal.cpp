/*=============================================================================
	signal.cpp : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

///////////////////////////////////////////////////////////////////////////////
#include <signal.h>
#include <errno.h>

///////////////////////////////////////////////////////////////////////////////
struct sigaction g_tcrt_sigaction_slot[SIGMAX];

///////////////////////////////////////////////////////////////////////////////
int sigaction(int sig, const struct sigaction* act, struct sigaction* oact)
{
	if (sig > 0 && sig < SIGMAX) {
		if (oact != NULL)
			*oact = g_tcrt_sigaction_slot[sig];
		g_tcrt_sigaction_slot[sig] = *act;
		return 0;
	}
	errno = ERANGE;
	return -1;
}

int sigemptyset(sigset_t* set)
{
	*set = 0;
	return 0;
}

void (*signal(int sig, void (*func)(int)))(int)
{
	void (*prev)(int);

	if (func == SIG_IGN)
		func = SIG_DFL;

	if (sig > 0 && sig < SIGMAX) {
		prev = g_tcrt_sigaction_slot[sig].sa_handler;
		g_tcrt_sigaction_slot[sig].sa_handler = func;
		return prev;
	}
//	if (sig == SIGINT) {
//
//	}
	errno = ERANGE;
	return SIG_ERR;
}

int raise(int sig)
{
	if (sig > 0 && sig < SIGMAX) {
		if (g_tcrt_sigaction_slot[sig].sa_handler != SIG_DFL)
			g_tcrt_sigaction_slot[sig].sa_handler(sig);
		return 0;
	}
	errno = ERANGE;
	return -1;
}

// EOF ////////////////////////////////////////////////////////////////////////
