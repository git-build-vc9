/*=============================================================================
	utils.h : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#ifndef TINYCRT_UTILS__H_
#define TINYCRT_UTILS__H_

///////////////////////////////////////////////////////////////////////////////
#include <sys/socket.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <assert.h>
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

///////////////////////////////////////////////////////////////////////////////
typedef signed char		schar;
typedef signed short		sshort;
typedef signed int		sint;
typedef signed long		slong;
typedef signed long long	slonglong;
typedef unsigned char		uchar;
typedef unsigned short		ushort;
typedef unsigned int		uint;
typedef unsigned long		ulong;
typedef unsigned long long	ulonglong;

///////////////////////////////////////////////////////////////////////////////
namespace tcrt {
	template<class T, class U> T div(T numerator, U denominator, U& remainder)
	{
		remainder = numerator % denominator;
		return numerator / denominator;
	}

	template<class T, class U> U mod(T numerator, U denominator, T& quotient)
	{
		quotient = numerator / denominator;
		return numerator % denominator;
	}

	inline HANDLE fildes_to_handle(int fildes)
	{
		if (fildes < 3)
			return GetStdHandle(STD_INPUT_HANDLE - fildes);
		return (HANDLE) (fildes & ~SOCKET_FILDES);
	}

	inline bool is_socket_fildes(int fildes)
	{
		return (fildes & SOCKET_FILDES);
	}

	inline bool is_console_handle(HANDLE h)
	{
		return (((size_t) h & 0x10000003) == 0x03);
	}

	inline size_t utf8_to_utf16(const char* src, size_t srclen, wchar_t* dst, size_t dstlen)
	{
		int len;

		len = MultiByteToWideChar(CP_UTF8, 0, src, srclen, dst, dstlen);
	#ifndef NDEBUG
		if (!len)
			__debugbreak();
	#endif
		return len;
	}

	inline size_t utf16_to_utf8(const wchar_t* src, size_t srclen, char* dst, size_t dstlen)
	{
		int len;

		len = WideCharToMultiByte(CP_UTF8, 0, src, srclen, dst, dstlen, NULL, NULL);
	#ifndef NDEBUG
		if (!len)
			__debugbreak();
	#endif
		return len;
	}

	template<class T> class buffer {
	public:
		buffer() : _ptr(NULL) {
		}
		~buffer() {
			free(_ptr);
		}

		bool resize(size_t len) {
			T* p = (T*) realloc(_ptr, len * sizeof(T));

			if (p == NULL) {
				errno = ENOMEM;
				return false;
			}
			_ptr = p;
			_len = len;
			return true;
		}
		size_t length() {
			return _len;
		}
		size_t size() {
			return _len * sizeof(T);
		}

		bool operator!() {
			return !_ptr;
		}
		T& operator*() {
			return *_ptr;
		}
		T& operator[](size_t idx) {
			assert(idx < _len);
			return _ptr[idx];
		}
		T* operator->() {
			return _ptr;
		}
		T* get() {
			return _ptr;
		}

	private:
		buffer(const buffer&);
		buffer& operator=(const buffer&);

		T*	_ptr;
		size_t	_len;
	};

	class rolock {
	public:
		rolock() {
			while (!InitializeCriticalSectionAndSpinCount(&_criticalSection, 4000))
				__debugbreak();
		}
		~rolock() {
			DeleteCriticalSection(&_criticalSection);
		}

		void acquire() {
			EnterCriticalSection(&_criticalSection);
		}
		bool try_acquired() {
			return (TryEnterCriticalSection(&_criticalSection) != FALSE);
		}
		void release() {
			LeaveCriticalSection(&_criticalSection);
		}

	private:
		CRITICAL_SECTION _criticalSection;
	};

	class auto_rolock {
	public:
		auto_rolock(rolock& lock) : _lock(lock)	{ _lock.acquire(); }
		~auto_rolock()				{ _lock.release(); }

	private:
		rolock& _lock;
	};

} /* namespace tcrt */

inline size_t tcrt_utf8_to_utf16(const char *src, size_t srclen, wchar_t *dst, size_t dstlen)
{
	return tcrt::utf8_to_utf16(src, srclen, dst, dstlen);
}

inline size_t tcrt_utf16_to_utf8(const wchar_t *src, size_t srclen, char *dst, size_t dstlen)
{
	return tcrt::utf16_to_utf8(src, srclen, dst, dstlen);
}

inline time_t filetime_to_time_t(const FILETIME *ft)
{
	LARGE_INTEGER time;

	time.LowPart = ft->dwLowDateTime;
	time.HighPart = ft->dwHighDateTime;
	time.QuadPart -= 116444736000000000ll;
	return (time_t) (time.QuadPart / 10000000);
}

#if _M_IX86
extern "C" long _InterlockedExchange(long volatile *, long);
extern "C" long _InterlockedCompareExchange(long volatile *, long, long);
#elif _M_X64
extern "C" __int64 _InterlockedExchange64(__int64 volatile *, __int64, __int64);
extern "C" __int64 _InterlockedCompareExchange64(__int64 volatile *, __int64, __int64);
#endif

template<class T> inline T* atomic_xchg(T** dst, T* value)
{
#if _M_IX86
	return (T*) _InterlockedExchange((long volatile *) dst, (long) value);
#elif _M_X64
	return (T*) _InterlockedExchange64((__int64 volatile *) dst, (__int64) value);
#endif
}

template<class T> inline T* atomic_cmpxchg(T** dst, T* value, T* cmp)
{
#if _M_IX86
	return (T*) _InterlockedCompareExchange((long volatile *) dst, (long) value, (long) cmp);
#elif _M_X64
	return (T*) _InterlockedCompareExchange64((__int64 volatile *) dst, (__int64) value, (__int64) cmp);
#endif
}

template<class T> inline T align_up(T value, const T alignment)
{
	return (value + alignment) & ~alignment;
}

template<class T, class U> inline T truncate_cast(U from)
{
	if (sizeof(T) < sizeof(U)) {
		if (((U)((T) from)) != from)
			__debugbreak();
	}
	return (T) from;
}

// EOF ////////////////////////////////////////////////////////////////////////
#endif /* TINYCRT_UTILS__H_ */
