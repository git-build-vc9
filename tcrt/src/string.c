/*=============================================================================
	string.c : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

///////////////////////////////////////////////////////////////////////////////
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <errno.h>

///////////////////////////////////////////////////////////////////////////////
char* strdup(const char* str)
{
	size_t len = strlen(str) + 1;
	char* p = malloc(len);

	return (p != NULL) ? memcpy(p, str, len) : NULL;
}

int stricmp(const char* str1, const char* str2)
{
	//
	// TODO: add unicode support
	//
	size_t s1len = strlen(str1);
	size_t s2len = strlen(str2);
	size_t len = (s1len < s2len) ? s1len : s2len;
	size_t i;

	for (i = 0; i < len; ++i) {
		int c1 = toupper(str1[i]);
		int c2 = toupper(str2[i]);

		if (c1 != c2)
			return c1 - c2;
	}
	return s1len - s2len;
}

int strnicmp(const char* str1, const char* str2, size_t len)
{
	//
	// TODO: add unicode support
	//
	size_t s1len = strnlen(str1, len);
	size_t s2len = strnlen(str2, len);
	size_t i;
	len = (s1len < s2len) ? s1len : s2len;

	for (i = 0; i < len; ++i) {
		int c1 = toupper(str1[i]);
		int c2 = toupper(str2[i]);

		if (c1 != c2)
			return c1 - c2;
	}
	return s1len - s2len;
}

int strcasecmp(const char *str1, const char *str2)
{
	return stricmp(str1, str2);
}

int strncasecmp(const char *str1, const char *str2, size_t len)
{
	return strnicmp(str1, str2, len);
}

char* strerror(int errnum)
{
	static TCRT_THREAD_LOCAL char buffer[20];

	sprintf(buffer, "errno %d", errnum);
	return buffer;
}

// EOF ////////////////////////////////////////////////////////////////////////
