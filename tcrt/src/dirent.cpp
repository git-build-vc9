/*=============================================================================
	dirent.cpp : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

///////////////////////////////////////////////////////////////////////////////
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include "utils.h"

///////////////////////////////////////////////////////////////////////////////
typedef struct _DIR {
	HANDLE			handle;
	WIN32_FIND_DATAW	next;
	tcrt::buffer<char>	name;
	struct dirent		entry;
	int			end;
} DIR;

///////////////////////////////////////////////////////////////////////////////
DIR* opendir(const char* dirname)
{
	tcrt::buffer<wchar_t> wname;
	size_t len = strlen(dirname) + 1;
	wchar_t* wn;
	DIR* dir;

	if (!wname.resize((len + 2) * sizeof(wchar_t)))
		return NULL;
	tcrt_utf8_to_utf16(dirname, len, wname.get(), len);
	wn = wname.get();
	if (wn[--len] != '/')
		wn[len++] = '/';
	wn[len++] = '*';
	wn[len] = '\0';
	dir = new DIR;
	if (dir == NULL) {
		errno = ENOMEM;
		return NULL;
	}
	dir->handle = FindFirstFileW(wname.get(), &dir->next);
	if (dir->handle == INVALID_HANDLE_VALUE) {
		tcrt_set_errno_win32();
		delete dir;
		return NULL;
	}
	memset(&dir->entry, 0, sizeof(dir->entry));
	dir->end = 0;

	return dir;
}

int closedir(DIR* dirp)
{
	HANDLE handle;

	handle = dirp->handle;
	delete dirp;
	if (!FindClose(handle)) {
		tcrt_set_errno_win32();
		return -1;
	}
	return 0;
}

struct dirent* readdir(DIR* dirp)
{
	size_t len;

	if (dirp->end)
		return NULL;

	len = wcslen(dirp->next.cFileName) + 1;
	if (!dirp->name.resize(len))
		return NULL;
	dirp->entry.d_name = dirp->name.get();
	tcrt_utf16_to_utf8(dirp->next.cFileName, len, dirp->name.get(), len);
	if (!FindNextFileW(dirp->handle, &dirp->next)) {
		if (GetLastError() == ERROR_NO_MORE_FILES) {
			dirp->end = 1;

		} else {
			tcrt_set_errno_win32();
			return NULL;
		}
	}
	dirp->entry.d_ino += 1;
	return &dirp->entry;
}

// EOF ////////////////////////////////////////////////////////////////////////
