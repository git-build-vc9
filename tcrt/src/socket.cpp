/*=============================================================================
	socket.cpp : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#include "Ws2_32.h"

///////////////////////////////////////////////////////////////////////////////
unsigned long ntohl(unsigned long netlong)
{
#if defined(_M_IX86) || defined(_M_X64)
	uchar* bs = (uchar*) &netlong;

	return ((ulong) bs[0] << 24)
		| ((ulong) bs[1] << 16)
		| ((ulong) bs[2] << 8)
		| ((ulong) bs[3]);
#else
#error	Unknown machine
#endif
}

unsigned long htonl(unsigned long hostlong)
{
#if defined(_M_IX86) || defined(_M_X64)
	uchar* bs = (uchar*) &hostlong;

	return ((ulong) bs[0] << 24)
		| ((ulong) bs[1] << 16)
		| ((ulong) bs[2] << 8)
		| ((ulong) bs[3]);
#else
#error	Unknown machine
#endif
}

unsigned short ntohs(unsigned short netshort)
{
#if defined(_M_IX86) || defined(_M_X64)
	uchar* bs = (uchar*) &netshort;

	return ((ushort) bs[0] << 8)
		| ((ushort) bs[1]);
#else
#error	Unknown machine
#endif
}

unsigned short htons(unsigned short hostshort)
{
#if defined(_M_IX86) || defined(_M_X64)
	uchar* bs = (uchar*) &hostshort;

	return ((ushort) bs[0] << 8)
		| ((ushort) bs[1]);
#else
#error	Unknown machine
#endif
}

int socket(int af, int type, int protocol)
{
	HANDLE sock;

	sock = Ws2_Socket(af, type, protocol, NULL, 0, 0x01);
	if (sock == INVALID_HANDLE_VALUE) {
		errno = Ws2_GetLastError();

	} else if ((size_t) sock > FOPEN_MAX) {
		Ws2_Close(sock);
		errno = ENFILE;

	} else {
		return (int) sock | SOCKET_FILDES;
	}
	return -1;
}

int accept(int sock, struct sockaddr* addr, size_t* addr_len)
{
	HANDLE sok;
	int len = truncate_cast<int>(*addr_len);

	__debugbreak();
	sok = Ws2_Accept(tcrt::fildes_to_handle(sock), addr, &len);
	if (sok == INVALID_HANDLE_VALUE) {
		errno = Ws2_GetLastError();

	} else if ((size_t) sok > FOPEN_MAX) {
		Ws2_Close(sok);
		errno = ENFILE;
		*addr_len = 0;

	} else {
		*addr_len = len;
		return (int) sok | SOCKET_FILDES;
	}
	return -1;
}

int bind(int sock, const struct sockaddr* addr, size_t addr_len)
{
	int len = truncate_cast<int>(addr_len);

	__debugbreak();
	if (Ws2_Bind(tcrt::fildes_to_handle(sock), addr, len)) {
		errno = Ws2_GetLastError();
		return -1;
	}
	return 0;
}

int connect(int sock, const struct sockaddr* addr, size_t addr_len)
{
	size_t len = truncate_cast<int>(addr_len);

	if (Ws2_Connect(tcrt::fildes_to_handle(sock), addr, addr_len)) {
		errno = Ws2_GetLastError();
		return -1;
	}
	return 0;
}

int listen(int sock, int backlog)
{
	__debugbreak();
	if (Ws2_Listen(tcrt::fildes_to_handle(sock), backlog)) {
		errno = Ws2_GetLastError();
		return -1;
	}
	return 0;
}

ssize_t recv(int sock, void* buf, size_t len, int flags)
{
	int blen = truncate_cast<int>(len);
	ssize_t res;

	res = Ws2_Recv(tcrt::fildes_to_handle(sock), buf, blen, flags);
	if (res < 0) {
		errno = Ws2_GetLastError();
		return -1;
	}
	return res;
}

ssize_t recvfrom(int sock, void* buf, size_t len, int flags, struct sockaddr* addr, size_t* addr_len)
{
	int blen = truncate_cast<int>(len);
	int alen = truncate_cast<int>(addr_len);
	ssize_t res;

	__debugbreak();
	res = Ws2_RecvFrom(tcrt::fildes_to_handle(sock), buf, blen, flags, addr, &alen);
	if (res < 0) {
		*addr_len = 0;
		errno = Ws2_GetLastError();
		return -1;
	}
	*addr_len = alen;
	return res;
}

ssize_t send(int sock, const void *buf, size_t len, int flags)
{
	int blen = truncate_cast<int>(len);
	ssize_t res;

	res = Ws2_Send(tcrt::fildes_to_handle(sock), buf, blen, flags);
	if (res < 0) {
		errno = Ws2_GetLastError();
		return -1;
	}
	return res;
}

ssize_t sendto(int sock, const void* buf, size_t len, int flags, const struct sockaddr* addr, size_t addr_len)
{
	int blen = truncate_cast<int>(len);
	int alen = truncate_cast<int>(addr_len);
	ssize_t res;

	__debugbreak();
	res = Ws2_SendTo(tcrt::fildes_to_handle(sock), buf, blen, flags, addr, alen);
	if (res < 0) {
		errno = Ws2_GetLastError();
		return -1;
	}
	return res;
}

#define _IOW(x, y, t)	(0x80000000 | (((long) sizeof(t) & 0x7F) << 16) | ((x) << 8) | (y))
#define FIONBIO		_IOW('f', 126, unsigned long)

int setsockopt(int sock, int level, int option, const void* buf, size_t len)
{
	if (level != SOL_SOCKET) {
		errno = EINVAL;
		return -1;
	}
	if (option == SO_NONBLOCK) {
		if (sizeof(len) != sizeof(unsigned long)) {
			__debugbreak();
			errno = EINVAL;
			return -1;
		}
		if (Ws2_Ioctl(tcrt::fildes_to_handle(sock), FIONBIO, (unsigned long*) buf)) {
			errno = Ws2_GetLastError();
			return -1;
		}

	} else if (Ws2_SetSocketOptions(tcrt::fildes_to_handle(sock), level, option, buf, len)) {
		errno = Ws2_GetLastError();
		return -1;
	}
	return 0;
}

int shutdown(int sock, int how)
{
	__debugbreak();
	errno = ENOSYS;
	return -1;
}

int getsockname(int sock, struct sockaddr* name, size_t* name_len)
{
	__debugbreak();
	errno = ENOSYS;
	return -1;
}

int gethostname(char* name, size_t len)
{
	if (Ws2_GetHostName(name, truncate_cast<int>(len))) {
		errno = Ws2_GetLastError();
		return -1;
	}
	return 0;
}

struct hostent* gethostbyname(const char* name)
{
	struct hostent* he;

	he = Ws2_GetHostByName(name);
	if (he == NULL)
		errno = Ws2_GetLastError();
	return he;
}

int getaddrinfo(const char* name, const char* service, const struct addrinfo* hints, struct addrinfo** res)
{
	tcrt::buffer<wchar_t> wname;
	tcrt::buffer<wchar_t> wservice;
	size_t namlen = strlen(name) + 1;
	struct addrinfo** new_ai;
	struct addrinfo* old_ai;
	struct addrinfo* ai;
	int ret;

	if (!wname.resize(namlen))
		return -1;
	if (service != NULL) {
		size_t srvlen = strlen(service) + 1;

		if (!wservice.resize(srvlen))
			return -1;
		tcrt_utf8_to_utf16(service, srvlen, wservice.get(), srvlen);
		service = (char*) wservice.get();
	}
	tcrt_utf8_to_utf16(name, namlen, wname.get(), namlen);
	ret = Ws2_GetAddressInfo(wname.get(), (wchar_t*) service, hints, &ai);
	if (ret) {
		*res = NULL;
		return ret;
	}

	old_ai = ai;
	new_ai = res;
	do {
		*new_ai = new struct addrinfo;
		if (*new_ai == NULL) {
			ret = 1;
			break;
		}
		memcpy(*new_ai, ai, sizeof(struct addrinfo));
		(*new_ai)->ai_next = NULL;
		(*new_ai)->ai_canonname = NULL;
		(*new_ai)->ai_addr = (struct sockaddr*) malloc(ai->ai_addrlen);
		if ((*new_ai)->ai_addr == NULL) {
			ret = 1;
			break;
		}
		memcpy((*new_ai)->ai_addr, ai->ai_addr, ai->ai_addrlen);
		if (ai->ai_canonname) {
			size_t len = wcslen((wchar_t*) ai->ai_canonname) + 1;

			(*new_ai)->ai_canonname = (char*) malloc(len * sizeof(wchar_t));
			if ((*new_ai)->ai_canonname == NULL) {
				ret = 1;
				break;
			}
			tcrt_utf16_to_utf8((wchar_t*) ai->ai_canonname, len, (*new_ai)->ai_canonname, len);
		}
		ai = ai->ai_next;
		new_ai = &(*new_ai)->ai_next;

	} while (ai);
	Ws2_FreeAddressInfo(old_ai);
	if (ret) {
		freeaddrinfo(*res);
		*res = NULL;
		return ret;
	}
	return 0;
}

void freeaddrinfo(struct addrinfo* res)
{
	struct addrinfo* ai;

	while (res) {
		ai = res;
		res = res->ai_next;
		free(ai->ai_canonname);
		free(ai->ai_addr);
		free(ai);
	}
}

const char* gai_strerror(int errcode)
{
	__debugbreak();
	errno = ENOSYS;
	return NULL;
}

const char* inet_ntop(int af, const void* src, char* dst, size_t size)
{
	__debugbreak();
	errno = ENOSYS;
	return NULL;
}

int inet_pton(int af, const char* src, void* dst)
{
	__debugbreak();
	errno = ENOSYS;
	return -1;
}

unsigned long inet_addr(const char* cp)
{
	return Ws2_InetAddr(cp);
}

char* inet_ntoa(struct in_addr in)
{
	char* res;

	res = Ws2_InetNtoa(in);
	if (res == NULL)
		errno = Ws2_GetLastError();
	return res;
}

// EOF ////////////////////////////////////////////////////////////////////////
