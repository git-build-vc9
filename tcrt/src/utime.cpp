/*=============================================================================
	utime.cpp : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#include <unistd.h>
#include <fcntl.h>
#include <utime.h>
#include <errno.h>
#include "utils.h"

///////////////////////////////////////////////////////////////////////////////
static inline FILETIME time_t_to_filetime(time_t t)
{
	LARGE_INTEGER time;
	FILETIME ft;

	time.QuadPart = t * 10000000ll + 116444736000000000ll;
	ft.dwLowDateTime = time.LowPart;
	ft.dwHighDateTime = time.HighPart;
	return ft;
}

int utime(const char* path, const struct utimbuf* times)
{
	FILETIME atime;
	FILETIME mtime;
	int fd;

	__debugbreak();
	if ((fd = open(path, O_RDWR)) < 0)
		return -1;
	atime = time_t_to_filetime(times->modtime);
	mtime = time_t_to_filetime(times->actime);
	if (!SetFileTime(tcrt::fildes_to_handle(fd), NULL, &atime, &mtime)) {
		close(fd);
		tcrt_set_errno_win32();
		return -1;
	}
	close(fd);
	return 0;
}

// EOF ////////////////////////////////////////////////////////////////////////
