/*=============================================================================
	stat.h : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include "utils.h"

////////////////////////////////////////////////////////////////////////////////
static inline int file_attr_to_st_mode(DWORD attr)
{
	int mode = S_IRUSR;

	if (attr & FILE_ATTRIBUTE_DIRECTORY)
		mode |= S_IFDIR;
	else if (attr & FILE_ATTRIBUTE_REPARSE_POINT)
		mode |= S_IFLNK;
	else
		mode |= S_IFREG;

	if (!(attr & FILE_ATTRIBUTE_READONLY))
		mode |= S_IWUSR;

	return mode;
}

int chmod(const char* path, mode_t mode)
{
	tcrt::buffer<wchar_t> wpath;
	size_t len;
	DWORD attr;

	len = strlen(path) + 1;
	if (!wpath.resize(len * sizeof(WCHAR)))
		return -1;

	tcrt_utf8_to_utf16(path, len, wpath.get(), len);
	attr = GetFileAttributesW(wpath.get());
	if (attr == INVALID_FILE_ATTRIBUTES) {
		tcrt_set_errno_win32();
		return -1;
	}
	if (mode & S_IWUSR)
		attr &= ~FILE_ATTRIBUTE_READONLY;
	else
		attr |= FILE_ATTRIBUTE_READONLY;
	if (!SetFileAttributesW(wpath.get(), attr)) {
		tcrt_set_errno_win32();
		return -1;
	}
	return 0;
}

int fchmod(int fildes, mode_t mode)
{
	FILE_BASIC_INFO binfo;
	FILETIME ctime;
	HANDLE handle;

	handle = tcrt::fildes_to_handle(fildes);
	if (!GetFileInformationByHandleEx(handle, FileBasicInfo, &binfo, sizeof(binfo))) {
		tcrt_set_errno_win32();
		return -1;
	}
	if (mode & S_IWUSR)
		binfo.FileAttributes &= ~FILE_ATTRIBUTE_READONLY;
	else
		binfo.FileAttributes |= FILE_ATTRIBUTE_READONLY;
	GetSystemTimeAsFileTime(&ctime);
	binfo.ChangeTime.LowPart = ctime.dwLowDateTime;
	binfo.ChangeTime.HighPart = ctime.dwHighDateTime;
	if (!SetFileInformationByHandle(handle, FileBasicInfo, &binfo, sizeof(binfo))) {
		tcrt_set_errno_win32();
		return -1;
	}
	return 0;
}

int fstat(int fildes, struct stat *buf)
{
	BY_HANDLE_FILE_INFORMATION info;
	HANDLE handle;

	handle = tcrt::fildes_to_handle(fildes);
	if (GetFileInformationByHandle(handle, &info)) {
		LARGE_INTEGER size;

		size.LowPart = info.nFileSizeLow;
		size.HighPart = info.nFileSizeHigh;
		buf->st_ino = 0;
		buf->st_gid = 0;
		buf->st_uid = 0; //getuid();
		buf->st_mode = file_attr_to_st_mode(info.dwFileAttributes);
		buf->st_size = size.QuadPart;
		buf->st_dev = 0;
		buf->st_atime = filetime_to_time_t(&info.ftLastAccessTime);
		buf->st_mtime = filetime_to_time_t(&info.ftLastWriteTime);
		buf->st_ctime = filetime_to_time_t(&info.ftCreationTime);
		buf->st_blksize = 4096;
		buf->st_blocks = ((buf->st_size + 4095) / 4096);
		return 0;

	} else if (fildes < 3 && handle != INVALID_HANDLE_VALUE) {
		memset(buf, 0, sizeof(*buf));
		return 0;
	}
	tcrt_set_errno_win32();
	return -1;
}

int mkdir(const char* path, mode_t mode)
{
	tcrt::buffer<wchar_t> wpath;
	size_t len;

	len = strlen(path) + 1;
	if (!wpath.resize(len * sizeof(wchar_t)))
		return -1;
	tcrt_utf8_to_utf16(path, len, wpath.get(), len);
	if (!CreateDirectoryW(wpath.get(), NULL)) {
		tcrt_set_errno_win32();
		return -1;
	}
	return 0;
}

int lstat(const char *path, struct stat *buf)
{
	//
	// FIXME
	//
	return stat(path, buf);
}

int stat(const char *path, struct stat *buf)
{
	int fd;
	int res;

	fd = open(path, O_RDONLY);
	if (fd < 0)
		return fd;
	res = fstat(fd, buf);
	close(fd);
	return res;
}

mode_t umask(mode_t mode)
{
	__debugbreak();
	errno = ENOSYS;
	return -1;
}

// EOF ////////////////////////////////////////////////////////////////////////
