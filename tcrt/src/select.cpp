/*=============================================================================
	select.cpp : 

	Copyright © 2008 Bruno Santos <nayart3@gmail.com>
=============================================================================*/

#include <sys/select.h>
#include <string.h>
#include <errno.h>
#include "Ws2_32.h"

///////////////////////////////////////////////////////////////////////////////
void FD_ZERO(fd_set* fs)
{
	memset(fs, 0, sizeof(fd_set));
}

void FD_SET(int fildes, fd_set* fs)
{
	HANDLE hd = tcrt::fildes_to_handle(fildes);

	if (fs->count >= FD_SETSIZE)
		return;
	for (uint i = 0; i < fs->count; ++i) {
		if (fs->fildes[i] == hd)
			return;
	}
	fs->fildes[fs->count++] = hd;
}

int FD_ISSET(int fildes, fd_set* fs)
{
	return Ws2_FDIsSet(tcrt::fildes_to_handle(fildes), fs);
}

int select(int nfds, fd_set* readfds, fd_set* writefds, fd_set* errorfds, struct timeval* timeout)
{
	int ret;

	if (!nfds) {
		DWORD msec = timeout->tv_sec * 1000 + timeout->tv_usec / 1000;
		Sleep(msec);
	}
	ret = Ws2_Select(nfds, readfds, writefds, errorfds, timeout);
	if (ret < 0) {
		errno = Ws2_GetLastError();
		return -1;
	}
	return ret;
}

// EOF ////////////////////////////////////////////////////////////////////////
